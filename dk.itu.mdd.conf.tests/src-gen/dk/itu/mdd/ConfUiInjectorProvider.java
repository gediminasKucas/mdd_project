/*
* generated by Xtext
*/
package dk.itu.mdd;

import org.eclipse.xtext.junit4.IInjectorProvider;

import com.google.inject.Injector;

public class ConfUiInjectorProvider implements IInjectorProvider {
	
	public Injector getInjector() {
		return dk.itu.mdd.ui.internal.ConfActivator.getInstance().getInjector("dk.itu.mdd.Conf");
	}
	
}
