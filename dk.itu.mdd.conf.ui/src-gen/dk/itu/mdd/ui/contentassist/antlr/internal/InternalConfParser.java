package dk.itu.mdd.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import dk.itu.mdd.services.ConfGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalConfParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'boolean'", "'string'", "'int'", "'char'", "'float'", "'double'", "'object'", "'range'", "'select'", "'multiSelect'", "'checkBox'", "'=='", "'!='", "'<'", "'>'", "'>='", "'<='", "'&&'", "'||'", "'True'", "'False'", "'('", "')'", "'availableOptions'", "'{'", "'}'", "','", "'depends'", "'option'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__37=37;
    public static final int T__16=16;
    public static final int T__38=38;
    public static final int T__17=17;
    public static final int T__39=39;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalConfParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalConfParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalConfParser.tokenNames; }
    public String getGrammarFileName() { return "../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g"; }


     
     	private ConfGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(ConfGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleModel"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:60:1: entryRuleModel : ruleModel EOF ;
    public final void entryRuleModel() throws RecognitionException {
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:61:1: ( ruleModel EOF )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:62:1: ruleModel EOF
            {
             before(grammarAccess.getModelRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleModel_in_entryRuleModel61);
            ruleModel();

            state._fsp--;

             after(grammarAccess.getModelRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleModel68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:69:1: ruleModel : ( ( rule__Model__Group__0 ) ) ;
    public final void ruleModel() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:73:2: ( ( ( rule__Model__Group__0 ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:74:1: ( ( rule__Model__Group__0 ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:74:1: ( ( rule__Model__Group__0 ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:75:1: ( rule__Model__Group__0 )
            {
             before(grammarAccess.getModelAccess().getGroup()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:76:1: ( rule__Model__Group__0 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:76:2: rule__Model__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Model__Group__0_in_ruleModel94);
            rule__Model__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getModelAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleDependency"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:88:1: entryRuleDependency : ruleDependency EOF ;
    public final void entryRuleDependency() throws RecognitionException {
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:89:1: ( ruleDependency EOF )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:90:1: ruleDependency EOF
            {
             before(grammarAccess.getDependencyRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleDependency_in_entryRuleDependency121);
            ruleDependency();

            state._fsp--;

             after(grammarAccess.getDependencyRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleDependency128); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDependency"


    // $ANTLR start "ruleDependency"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:97:1: ruleDependency : ( ( rule__Dependency__Group__0 ) ) ;
    public final void ruleDependency() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:101:2: ( ( ( rule__Dependency__Group__0 ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:102:1: ( ( rule__Dependency__Group__0 ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:102:1: ( ( rule__Dependency__Group__0 ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:103:1: ( rule__Dependency__Group__0 )
            {
             before(grammarAccess.getDependencyAccess().getGroup()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:104:1: ( rule__Dependency__Group__0 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:104:2: rule__Dependency__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Dependency__Group__0_in_ruleDependency154);
            rule__Dependency__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDependencyAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDependency"


    // $ANTLR start "entryRuleTerm"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:116:1: entryRuleTerm : ruleTerm EOF ;
    public final void entryRuleTerm() throws RecognitionException {
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:117:1: ( ruleTerm EOF )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:118:1: ruleTerm EOF
            {
             before(grammarAccess.getTermRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleTerm_in_entryRuleTerm181);
            ruleTerm();

            state._fsp--;

             after(grammarAccess.getTermRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleTerm188); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTerm"


    // $ANTLR start "ruleTerm"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:125:1: ruleTerm : ( ( rule__Term__Alternatives ) ) ;
    public final void ruleTerm() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:129:2: ( ( ( rule__Term__Alternatives ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:130:1: ( ( rule__Term__Alternatives ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:130:1: ( ( rule__Term__Alternatives ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:131:1: ( rule__Term__Alternatives )
            {
             before(grammarAccess.getTermAccess().getAlternatives()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:132:1: ( rule__Term__Alternatives )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:132:2: rule__Term__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__Term__Alternatives_in_ruleTerm214);
            rule__Term__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getTermAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTerm"


    // $ANTLR start "entryRuleComponent"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:144:1: entryRuleComponent : ruleComponent EOF ;
    public final void entryRuleComponent() throws RecognitionException {
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:145:1: ( ruleComponent EOF )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:146:1: ruleComponent EOF
            {
             before(grammarAccess.getComponentRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleComponent_in_entryRuleComponent241);
            ruleComponent();

            state._fsp--;

             after(grammarAccess.getComponentRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleComponent248); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleComponent"


    // $ANTLR start "ruleComponent"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:153:1: ruleComponent : ( ( rule__Component__Group__0 ) ) ;
    public final void ruleComponent() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:157:2: ( ( ( rule__Component__Group__0 ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:158:1: ( ( rule__Component__Group__0 ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:158:1: ( ( rule__Component__Group__0 ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:159:1: ( rule__Component__Group__0 )
            {
             before(grammarAccess.getComponentAccess().getGroup()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:160:1: ( rule__Component__Group__0 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:160:2: rule__Component__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Component__Group__0_in_ruleComponent274);
            rule__Component__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getComponentAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleComponent"


    // $ANTLR start "entryRuleOptions"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:172:1: entryRuleOptions : ruleOptions EOF ;
    public final void entryRuleOptions() throws RecognitionException {
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:173:1: ( ruleOptions EOF )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:174:1: ruleOptions EOF
            {
             before(grammarAccess.getOptionsRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleOptions_in_entryRuleOptions301);
            ruleOptions();

            state._fsp--;

             after(grammarAccess.getOptionsRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleOptions308); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOptions"


    // $ANTLR start "ruleOptions"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:181:1: ruleOptions : ( ( rule__Options__Group__0 ) ) ;
    public final void ruleOptions() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:185:2: ( ( ( rule__Options__Group__0 ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:186:1: ( ( rule__Options__Group__0 ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:186:1: ( ( rule__Options__Group__0 ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:187:1: ( rule__Options__Group__0 )
            {
             before(grammarAccess.getOptionsAccess().getGroup()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:188:1: ( rule__Options__Group__0 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:188:2: rule__Options__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Options__Group__0_in_ruleOptions334);
            rule__Options__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOptionsAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOptions"


    // $ANTLR start "entryRuleDependencyStatement"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:200:1: entryRuleDependencyStatement : ruleDependencyStatement EOF ;
    public final void entryRuleDependencyStatement() throws RecognitionException {
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:201:1: ( ruleDependencyStatement EOF )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:202:1: ruleDependencyStatement EOF
            {
             before(grammarAccess.getDependencyStatementRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleDependencyStatement_in_entryRuleDependencyStatement361);
            ruleDependencyStatement();

            state._fsp--;

             after(grammarAccess.getDependencyStatementRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleDependencyStatement368); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDependencyStatement"


    // $ANTLR start "ruleDependencyStatement"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:209:1: ruleDependencyStatement : ( ( rule__DependencyStatement__Group__0 ) ) ;
    public final void ruleDependencyStatement() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:213:2: ( ( ( rule__DependencyStatement__Group__0 ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:214:1: ( ( rule__DependencyStatement__Group__0 ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:214:1: ( ( rule__DependencyStatement__Group__0 ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:215:1: ( rule__DependencyStatement__Group__0 )
            {
             before(grammarAccess.getDependencyStatementAccess().getGroup()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:216:1: ( rule__DependencyStatement__Group__0 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:216:2: rule__DependencyStatement__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__DependencyStatement__Group__0_in_ruleDependencyStatement394);
            rule__DependencyStatement__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDependencyStatementAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDependencyStatement"


    // $ANTLR start "entryRuleEString"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:228:1: entryRuleEString : ruleEString EOF ;
    public final void entryRuleEString() throws RecognitionException {
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:229:1: ( ruleEString EOF )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:230:1: ruleEString EOF
            {
             before(grammarAccess.getEStringRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_entryRuleEString421);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEStringRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleEString428); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:237:1: ruleEString : ( ( rule__EString__Alternatives ) ) ;
    public final void ruleEString() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:241:2: ( ( ( rule__EString__Alternatives ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:242:1: ( ( rule__EString__Alternatives ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:242:1: ( ( rule__EString__Alternatives ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:243:1: ( rule__EString__Alternatives )
            {
             before(grammarAccess.getEStringAccess().getAlternatives()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:244:1: ( rule__EString__Alternatives )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:244:2: rule__EString__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__EString__Alternatives_in_ruleEString454);
            rule__EString__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getEStringAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleUnDep"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:258:1: entryRuleUnDep : ruleUnDep EOF ;
    public final void entryRuleUnDep() throws RecognitionException {
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:259:1: ( ruleUnDep EOF )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:260:1: ruleUnDep EOF
            {
             before(grammarAccess.getUnDepRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleUnDep_in_entryRuleUnDep483);
            ruleUnDep();

            state._fsp--;

             after(grammarAccess.getUnDepRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleUnDep490); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleUnDep"


    // $ANTLR start "ruleUnDep"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:267:1: ruleUnDep : ( ( rule__UnDep__Group__0 ) ) ;
    public final void ruleUnDep() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:271:2: ( ( ( rule__UnDep__Group__0 ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:272:1: ( ( rule__UnDep__Group__0 ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:272:1: ( ( rule__UnDep__Group__0 ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:273:1: ( rule__UnDep__Group__0 )
            {
             before(grammarAccess.getUnDepAccess().getGroup()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:274:1: ( rule__UnDep__Group__0 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:274:2: rule__UnDep__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__UnDep__Group__0_in_ruleUnDep516);
            rule__UnDep__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getUnDepAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUnDep"


    // $ANTLR start "entryRuleConst"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:286:1: entryRuleConst : ruleConst EOF ;
    public final void entryRuleConst() throws RecognitionException {
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:287:1: ( ruleConst EOF )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:288:1: ruleConst EOF
            {
             before(grammarAccess.getConstRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleConst_in_entryRuleConst543);
            ruleConst();

            state._fsp--;

             after(grammarAccess.getConstRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleConst550); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConst"


    // $ANTLR start "ruleConst"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:295:1: ruleConst : ( ( rule__Const__Alternatives ) ) ;
    public final void ruleConst() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:299:2: ( ( ( rule__Const__Alternatives ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:300:1: ( ( rule__Const__Alternatives ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:300:1: ( ( rule__Const__Alternatives ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:301:1: ( rule__Const__Alternatives )
            {
             before(grammarAccess.getConstAccess().getAlternatives()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:302:1: ( rule__Const__Alternatives )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:302:2: rule__Const__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__Const__Alternatives_in_ruleConst576);
            rule__Const__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getConstAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConst"


    // $ANTLR start "ruleSimpleTypes"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:315:1: ruleSimpleTypes : ( ( rule__SimpleTypes__Alternatives ) ) ;
    public final void ruleSimpleTypes() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:319:1: ( ( ( rule__SimpleTypes__Alternatives ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:320:1: ( ( rule__SimpleTypes__Alternatives ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:320:1: ( ( rule__SimpleTypes__Alternatives ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:321:1: ( rule__SimpleTypes__Alternatives )
            {
             before(grammarAccess.getSimpleTypesAccess().getAlternatives()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:322:1: ( rule__SimpleTypes__Alternatives )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:322:2: rule__SimpleTypes__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__SimpleTypes__Alternatives_in_ruleSimpleTypes613);
            rule__SimpleTypes__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getSimpleTypesAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSimpleTypes"


    // $ANTLR start "ruleDisplayType"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:334:1: ruleDisplayType : ( ( rule__DisplayType__Alternatives ) ) ;
    public final void ruleDisplayType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:338:1: ( ( ( rule__DisplayType__Alternatives ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:339:1: ( ( rule__DisplayType__Alternatives ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:339:1: ( ( rule__DisplayType__Alternatives ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:340:1: ( rule__DisplayType__Alternatives )
            {
             before(grammarAccess.getDisplayTypeAccess().getAlternatives()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:341:1: ( rule__DisplayType__Alternatives )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:341:2: rule__DisplayType__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__DisplayType__Alternatives_in_ruleDisplayType649);
            rule__DisplayType__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getDisplayTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDisplayType"


    // $ANTLR start "ruleBinOperator"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:353:1: ruleBinOperator : ( ( rule__BinOperator__Alternatives ) ) ;
    public final void ruleBinOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:357:1: ( ( ( rule__BinOperator__Alternatives ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:358:1: ( ( rule__BinOperator__Alternatives ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:358:1: ( ( rule__BinOperator__Alternatives ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:359:1: ( rule__BinOperator__Alternatives )
            {
             before(grammarAccess.getBinOperatorAccess().getAlternatives()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:360:1: ( rule__BinOperator__Alternatives )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:360:2: rule__BinOperator__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__BinOperator__Alternatives_in_ruleBinOperator685);
            rule__BinOperator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getBinOperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBinOperator"


    // $ANTLR start "ruleUnOperator"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:372:1: ruleUnOperator : ( ( rule__UnOperator__Alternatives ) ) ;
    public final void ruleUnOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:376:1: ( ( ( rule__UnOperator__Alternatives ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:377:1: ( ( rule__UnOperator__Alternatives ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:377:1: ( ( rule__UnOperator__Alternatives ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:378:1: ( rule__UnOperator__Alternatives )
            {
             before(grammarAccess.getUnOperatorAccess().getAlternatives()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:379:1: ( rule__UnOperator__Alternatives )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:379:2: rule__UnOperator__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__UnOperator__Alternatives_in_ruleUnOperator721);
            rule__UnOperator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getUnOperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUnOperator"


    // $ANTLR start "rule__Term__Alternatives"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:390:1: rule__Term__Alternatives : ( ( ( rule__Term__Group_0__0 ) ) | ( ruleUnDep ) | ( ruleConst ) );
    public final void rule__Term__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:394:1: ( ( ( rule__Term__Group_0__0 ) ) | ( ruleUnDep ) | ( ruleConst ) )
            int alt1=3;
            switch ( input.LA(1) ) {
            case 32:
                {
                alt1=1;
                }
                break;
            case 30:
            case 31:
                {
                alt1=2;
                }
                break;
            case RULE_ID:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
                {
                alt1=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:395:1: ( ( rule__Term__Group_0__0 ) )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:395:1: ( ( rule__Term__Group_0__0 ) )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:396:1: ( rule__Term__Group_0__0 )
                    {
                     before(grammarAccess.getTermAccess().getGroup_0()); 
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:397:1: ( rule__Term__Group_0__0 )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:397:2: rule__Term__Group_0__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Term__Group_0__0_in_rule__Term__Alternatives756);
                    rule__Term__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getTermAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:401:6: ( ruleUnDep )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:401:6: ( ruleUnDep )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:402:1: ruleUnDep
                    {
                     before(grammarAccess.getTermAccess().getUnDepParserRuleCall_1()); 
                    pushFollow(FollowSets000.FOLLOW_ruleUnDep_in_rule__Term__Alternatives774);
                    ruleUnDep();

                    state._fsp--;

                     after(grammarAccess.getTermAccess().getUnDepParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:407:6: ( ruleConst )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:407:6: ( ruleConst )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:408:1: ruleConst
                    {
                     before(grammarAccess.getTermAccess().getConstParserRuleCall_2()); 
                    pushFollow(FollowSets000.FOLLOW_ruleConst_in_rule__Term__Alternatives791);
                    ruleConst();

                    state._fsp--;

                     after(grammarAccess.getTermAccess().getConstParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__Alternatives"


    // $ANTLR start "rule__Component__Alternatives_2"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:418:1: rule__Component__Alternatives_2 : ( ( ( rule__Component__ValueAssignment_2_0 ) ) | ( ( rule__Component__Group_2_1__0 ) ) );
    public final void rule__Component__Alternatives_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:422:1: ( ( ( rule__Component__ValueAssignment_2_0 ) ) | ( ( rule__Component__Group_2_1__0 ) ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( ((LA2_0>=RULE_STRING && LA2_0<=RULE_ID)) ) {
                alt2=1;
            }
            else if ( (LA2_0==34) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:423:1: ( ( rule__Component__ValueAssignment_2_0 ) )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:423:1: ( ( rule__Component__ValueAssignment_2_0 ) )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:424:1: ( rule__Component__ValueAssignment_2_0 )
                    {
                     before(grammarAccess.getComponentAccess().getValueAssignment_2_0()); 
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:425:1: ( rule__Component__ValueAssignment_2_0 )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:425:2: rule__Component__ValueAssignment_2_0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Component__ValueAssignment_2_0_in_rule__Component__Alternatives_2823);
                    rule__Component__ValueAssignment_2_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getComponentAccess().getValueAssignment_2_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:429:6: ( ( rule__Component__Group_2_1__0 ) )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:429:6: ( ( rule__Component__Group_2_1__0 ) )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:430:1: ( rule__Component__Group_2_1__0 )
                    {
                     before(grammarAccess.getComponentAccess().getGroup_2_1()); 
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:431:1: ( rule__Component__Group_2_1__0 )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:431:2: rule__Component__Group_2_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Component__Group_2_1__0_in_rule__Component__Alternatives_2841);
                    rule__Component__Group_2_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getComponentAccess().getGroup_2_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Alternatives_2"


    // $ANTLR start "rule__EString__Alternatives"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:440:1: rule__EString__Alternatives : ( ( RULE_STRING ) | ( RULE_ID ) );
    public final void rule__EString__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:444:1: ( ( RULE_STRING ) | ( RULE_ID ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_STRING) ) {
                alt3=1;
            }
            else if ( (LA3_0==RULE_ID) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:445:1: ( RULE_STRING )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:445:1: ( RULE_STRING )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:446:1: RULE_STRING
                    {
                     before(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                    match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_rule__EString__Alternatives874); 
                     after(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:451:6: ( RULE_ID )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:451:6: ( RULE_ID )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:452:1: RULE_ID
                    {
                     before(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                    match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__EString__Alternatives891); 
                     after(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Alternatives"


    // $ANTLR start "rule__Const__Alternatives"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:462:1: rule__Const__Alternatives : ( ( ( rule__Const__Group_0__0 ) ) | ( ( rule__Const__ComponentAssignment_1 ) ) );
    public final void rule__Const__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:466:1: ( ( ( rule__Const__Group_0__0 ) ) | ( ( rule__Const__ComponentAssignment_1 ) ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( ((LA4_0>=11 && LA4_0<=17)) ) {
                alt4=1;
            }
            else if ( (LA4_0==RULE_ID) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:467:1: ( ( rule__Const__Group_0__0 ) )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:467:1: ( ( rule__Const__Group_0__0 ) )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:468:1: ( rule__Const__Group_0__0 )
                    {
                     before(grammarAccess.getConstAccess().getGroup_0()); 
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:469:1: ( rule__Const__Group_0__0 )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:469:2: rule__Const__Group_0__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Const__Group_0__0_in_rule__Const__Alternatives923);
                    rule__Const__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getConstAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:473:6: ( ( rule__Const__ComponentAssignment_1 ) )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:473:6: ( ( rule__Const__ComponentAssignment_1 ) )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:474:1: ( rule__Const__ComponentAssignment_1 )
                    {
                     before(grammarAccess.getConstAccess().getComponentAssignment_1()); 
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:475:1: ( rule__Const__ComponentAssignment_1 )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:475:2: rule__Const__ComponentAssignment_1
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Const__ComponentAssignment_1_in_rule__Const__Alternatives941);
                    rule__Const__ComponentAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getConstAccess().getComponentAssignment_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Const__Alternatives"


    // $ANTLR start "rule__SimpleTypes__Alternatives"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:484:1: rule__SimpleTypes__Alternatives : ( ( ( 'boolean' ) ) | ( ( 'string' ) ) | ( ( 'int' ) ) | ( ( 'char' ) ) | ( ( 'float' ) ) | ( ( 'double' ) ) | ( ( 'object' ) ) );
    public final void rule__SimpleTypes__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:488:1: ( ( ( 'boolean' ) ) | ( ( 'string' ) ) | ( ( 'int' ) ) | ( ( 'char' ) ) | ( ( 'float' ) ) | ( ( 'double' ) ) | ( ( 'object' ) ) )
            int alt5=7;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt5=1;
                }
                break;
            case 12:
                {
                alt5=2;
                }
                break;
            case 13:
                {
                alt5=3;
                }
                break;
            case 14:
                {
                alt5=4;
                }
                break;
            case 15:
                {
                alt5=5;
                }
                break;
            case 16:
                {
                alt5=6;
                }
                break;
            case 17:
                {
                alt5=7;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:489:1: ( ( 'boolean' ) )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:489:1: ( ( 'boolean' ) )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:490:1: ( 'boolean' )
                    {
                     before(grammarAccess.getSimpleTypesAccess().getBooleanEnumLiteralDeclaration_0()); 
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:491:1: ( 'boolean' )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:491:3: 'boolean'
                    {
                    match(input,11,FollowSets000.FOLLOW_11_in_rule__SimpleTypes__Alternatives975); 

                    }

                     after(grammarAccess.getSimpleTypesAccess().getBooleanEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:496:6: ( ( 'string' ) )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:496:6: ( ( 'string' ) )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:497:1: ( 'string' )
                    {
                     before(grammarAccess.getSimpleTypesAccess().getStringEnumLiteralDeclaration_1()); 
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:498:1: ( 'string' )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:498:3: 'string'
                    {
                    match(input,12,FollowSets000.FOLLOW_12_in_rule__SimpleTypes__Alternatives996); 

                    }

                     after(grammarAccess.getSimpleTypesAccess().getStringEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:503:6: ( ( 'int' ) )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:503:6: ( ( 'int' ) )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:504:1: ( 'int' )
                    {
                     before(grammarAccess.getSimpleTypesAccess().getIntEnumLiteralDeclaration_2()); 
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:505:1: ( 'int' )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:505:3: 'int'
                    {
                    match(input,13,FollowSets000.FOLLOW_13_in_rule__SimpleTypes__Alternatives1017); 

                    }

                     after(grammarAccess.getSimpleTypesAccess().getIntEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:510:6: ( ( 'char' ) )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:510:6: ( ( 'char' ) )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:511:1: ( 'char' )
                    {
                     before(grammarAccess.getSimpleTypesAccess().getCharEnumLiteralDeclaration_3()); 
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:512:1: ( 'char' )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:512:3: 'char'
                    {
                    match(input,14,FollowSets000.FOLLOW_14_in_rule__SimpleTypes__Alternatives1038); 

                    }

                     after(grammarAccess.getSimpleTypesAccess().getCharEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;
                case 5 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:517:6: ( ( 'float' ) )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:517:6: ( ( 'float' ) )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:518:1: ( 'float' )
                    {
                     before(grammarAccess.getSimpleTypesAccess().getFloatEnumLiteralDeclaration_4()); 
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:519:1: ( 'float' )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:519:3: 'float'
                    {
                    match(input,15,FollowSets000.FOLLOW_15_in_rule__SimpleTypes__Alternatives1059); 

                    }

                     after(grammarAccess.getSimpleTypesAccess().getFloatEnumLiteralDeclaration_4()); 

                    }


                    }
                    break;
                case 6 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:524:6: ( ( 'double' ) )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:524:6: ( ( 'double' ) )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:525:1: ( 'double' )
                    {
                     before(grammarAccess.getSimpleTypesAccess().getDoubleEnumLiteralDeclaration_5()); 
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:526:1: ( 'double' )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:526:3: 'double'
                    {
                    match(input,16,FollowSets000.FOLLOW_16_in_rule__SimpleTypes__Alternatives1080); 

                    }

                     after(grammarAccess.getSimpleTypesAccess().getDoubleEnumLiteralDeclaration_5()); 

                    }


                    }
                    break;
                case 7 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:531:6: ( ( 'object' ) )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:531:6: ( ( 'object' ) )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:532:1: ( 'object' )
                    {
                     before(grammarAccess.getSimpleTypesAccess().getObjectEnumLiteralDeclaration_6()); 
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:533:1: ( 'object' )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:533:3: 'object'
                    {
                    match(input,17,FollowSets000.FOLLOW_17_in_rule__SimpleTypes__Alternatives1101); 

                    }

                     after(grammarAccess.getSimpleTypesAccess().getObjectEnumLiteralDeclaration_6()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleTypes__Alternatives"


    // $ANTLR start "rule__DisplayType__Alternatives"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:543:1: rule__DisplayType__Alternatives : ( ( ( 'range' ) ) | ( ( 'select' ) ) | ( ( 'multiSelect' ) ) | ( ( 'checkBox' ) ) );
    public final void rule__DisplayType__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:547:1: ( ( ( 'range' ) ) | ( ( 'select' ) ) | ( ( 'multiSelect' ) ) | ( ( 'checkBox' ) ) )
            int alt6=4;
            switch ( input.LA(1) ) {
            case 18:
                {
                alt6=1;
                }
                break;
            case 19:
                {
                alt6=2;
                }
                break;
            case 20:
                {
                alt6=3;
                }
                break;
            case 21:
                {
                alt6=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:548:1: ( ( 'range' ) )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:548:1: ( ( 'range' ) )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:549:1: ( 'range' )
                    {
                     before(grammarAccess.getDisplayTypeAccess().getRangeEnumLiteralDeclaration_0()); 
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:550:1: ( 'range' )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:550:3: 'range'
                    {
                    match(input,18,FollowSets000.FOLLOW_18_in_rule__DisplayType__Alternatives1137); 

                    }

                     after(grammarAccess.getDisplayTypeAccess().getRangeEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:555:6: ( ( 'select' ) )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:555:6: ( ( 'select' ) )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:556:1: ( 'select' )
                    {
                     before(grammarAccess.getDisplayTypeAccess().getSelectEnumLiteralDeclaration_1()); 
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:557:1: ( 'select' )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:557:3: 'select'
                    {
                    match(input,19,FollowSets000.FOLLOW_19_in_rule__DisplayType__Alternatives1158); 

                    }

                     after(grammarAccess.getDisplayTypeAccess().getSelectEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:562:6: ( ( 'multiSelect' ) )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:562:6: ( ( 'multiSelect' ) )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:563:1: ( 'multiSelect' )
                    {
                     before(grammarAccess.getDisplayTypeAccess().getMultiSelectEnumLiteralDeclaration_2()); 
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:564:1: ( 'multiSelect' )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:564:3: 'multiSelect'
                    {
                    match(input,20,FollowSets000.FOLLOW_20_in_rule__DisplayType__Alternatives1179); 

                    }

                     after(grammarAccess.getDisplayTypeAccess().getMultiSelectEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:569:6: ( ( 'checkBox' ) )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:569:6: ( ( 'checkBox' ) )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:570:1: ( 'checkBox' )
                    {
                     before(grammarAccess.getDisplayTypeAccess().getCheckBoxEnumLiteralDeclaration_3()); 
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:571:1: ( 'checkBox' )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:571:3: 'checkBox'
                    {
                    match(input,21,FollowSets000.FOLLOW_21_in_rule__DisplayType__Alternatives1200); 

                    }

                     after(grammarAccess.getDisplayTypeAccess().getCheckBoxEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisplayType__Alternatives"


    // $ANTLR start "rule__BinOperator__Alternatives"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:581:1: rule__BinOperator__Alternatives : ( ( ( '==' ) ) | ( ( '!=' ) ) | ( ( '<' ) ) | ( ( '>' ) ) | ( ( '>=' ) ) | ( ( '<=' ) ) | ( ( '&&' ) ) | ( ( '||' ) ) );
    public final void rule__BinOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:585:1: ( ( ( '==' ) ) | ( ( '!=' ) ) | ( ( '<' ) ) | ( ( '>' ) ) | ( ( '>=' ) ) | ( ( '<=' ) ) | ( ( '&&' ) ) | ( ( '||' ) ) )
            int alt7=8;
            switch ( input.LA(1) ) {
            case 22:
                {
                alt7=1;
                }
                break;
            case 23:
                {
                alt7=2;
                }
                break;
            case 24:
                {
                alt7=3;
                }
                break;
            case 25:
                {
                alt7=4;
                }
                break;
            case 26:
                {
                alt7=5;
                }
                break;
            case 27:
                {
                alt7=6;
                }
                break;
            case 28:
                {
                alt7=7;
                }
                break;
            case 29:
                {
                alt7=8;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }

            switch (alt7) {
                case 1 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:586:1: ( ( '==' ) )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:586:1: ( ( '==' ) )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:587:1: ( '==' )
                    {
                     before(grammarAccess.getBinOperatorAccess().getEqualEnumLiteralDeclaration_0()); 
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:588:1: ( '==' )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:588:3: '=='
                    {
                    match(input,22,FollowSets000.FOLLOW_22_in_rule__BinOperator__Alternatives1236); 

                    }

                     after(grammarAccess.getBinOperatorAccess().getEqualEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:593:6: ( ( '!=' ) )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:593:6: ( ( '!=' ) )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:594:1: ( '!=' )
                    {
                     before(grammarAccess.getBinOperatorAccess().getNotEqualEnumLiteralDeclaration_1()); 
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:595:1: ( '!=' )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:595:3: '!='
                    {
                    match(input,23,FollowSets000.FOLLOW_23_in_rule__BinOperator__Alternatives1257); 

                    }

                     after(grammarAccess.getBinOperatorAccess().getNotEqualEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:600:6: ( ( '<' ) )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:600:6: ( ( '<' ) )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:601:1: ( '<' )
                    {
                     before(grammarAccess.getBinOperatorAccess().getLessEnumLiteralDeclaration_2()); 
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:602:1: ( '<' )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:602:3: '<'
                    {
                    match(input,24,FollowSets000.FOLLOW_24_in_rule__BinOperator__Alternatives1278); 

                    }

                     after(grammarAccess.getBinOperatorAccess().getLessEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:607:6: ( ( '>' ) )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:607:6: ( ( '>' ) )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:608:1: ( '>' )
                    {
                     before(grammarAccess.getBinOperatorAccess().getGreaterEnumLiteralDeclaration_3()); 
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:609:1: ( '>' )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:609:3: '>'
                    {
                    match(input,25,FollowSets000.FOLLOW_25_in_rule__BinOperator__Alternatives1299); 

                    }

                     after(grammarAccess.getBinOperatorAccess().getGreaterEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;
                case 5 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:614:6: ( ( '>=' ) )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:614:6: ( ( '>=' ) )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:615:1: ( '>=' )
                    {
                     before(grammarAccess.getBinOperatorAccess().getGreaterEqualEnumLiteralDeclaration_4()); 
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:616:1: ( '>=' )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:616:3: '>='
                    {
                    match(input,26,FollowSets000.FOLLOW_26_in_rule__BinOperator__Alternatives1320); 

                    }

                     after(grammarAccess.getBinOperatorAccess().getGreaterEqualEnumLiteralDeclaration_4()); 

                    }


                    }
                    break;
                case 6 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:621:6: ( ( '<=' ) )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:621:6: ( ( '<=' ) )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:622:1: ( '<=' )
                    {
                     before(grammarAccess.getBinOperatorAccess().getLessEqualEnumLiteralDeclaration_5()); 
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:623:1: ( '<=' )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:623:3: '<='
                    {
                    match(input,27,FollowSets000.FOLLOW_27_in_rule__BinOperator__Alternatives1341); 

                    }

                     after(grammarAccess.getBinOperatorAccess().getLessEqualEnumLiteralDeclaration_5()); 

                    }


                    }
                    break;
                case 7 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:628:6: ( ( '&&' ) )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:628:6: ( ( '&&' ) )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:629:1: ( '&&' )
                    {
                     before(grammarAccess.getBinOperatorAccess().getAndEnumLiteralDeclaration_6()); 
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:630:1: ( '&&' )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:630:3: '&&'
                    {
                    match(input,28,FollowSets000.FOLLOW_28_in_rule__BinOperator__Alternatives1362); 

                    }

                     after(grammarAccess.getBinOperatorAccess().getAndEnumLiteralDeclaration_6()); 

                    }


                    }
                    break;
                case 8 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:635:6: ( ( '||' ) )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:635:6: ( ( '||' ) )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:636:1: ( '||' )
                    {
                     before(grammarAccess.getBinOperatorAccess().getOrEnumLiteralDeclaration_7()); 
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:637:1: ( '||' )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:637:3: '||'
                    {
                    match(input,29,FollowSets000.FOLLOW_29_in_rule__BinOperator__Alternatives1383); 

                    }

                     after(grammarAccess.getBinOperatorAccess().getOrEnumLiteralDeclaration_7()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinOperator__Alternatives"


    // $ANTLR start "rule__UnOperator__Alternatives"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:647:1: rule__UnOperator__Alternatives : ( ( ( 'True' ) ) | ( ( 'False' ) ) );
    public final void rule__UnOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:651:1: ( ( ( 'True' ) ) | ( ( 'False' ) ) )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==30) ) {
                alt8=1;
            }
            else if ( (LA8_0==31) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:652:1: ( ( 'True' ) )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:652:1: ( ( 'True' ) )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:653:1: ( 'True' )
                    {
                     before(grammarAccess.getUnOperatorAccess().getTrueEnumLiteralDeclaration_0()); 
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:654:1: ( 'True' )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:654:3: 'True'
                    {
                    match(input,30,FollowSets000.FOLLOW_30_in_rule__UnOperator__Alternatives1419); 

                    }

                     after(grammarAccess.getUnOperatorAccess().getTrueEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:659:6: ( ( 'False' ) )
                    {
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:659:6: ( ( 'False' ) )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:660:1: ( 'False' )
                    {
                     before(grammarAccess.getUnOperatorAccess().getFalseEnumLiteralDeclaration_1()); 
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:661:1: ( 'False' )
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:661:3: 'False'
                    {
                    match(input,31,FollowSets000.FOLLOW_31_in_rule__UnOperator__Alternatives1440); 

                    }

                     after(grammarAccess.getUnOperatorAccess().getFalseEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnOperator__Alternatives"


    // $ANTLR start "rule__Model__Group__0"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:673:1: rule__Model__Group__0 : rule__Model__Group__0__Impl rule__Model__Group__1 ;
    public final void rule__Model__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:677:1: ( rule__Model__Group__0__Impl rule__Model__Group__1 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:678:2: rule__Model__Group__0__Impl rule__Model__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Model__Group__0__Impl_in_rule__Model__Group__01473);
            rule__Model__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Model__Group__1_in_rule__Model__Group__01476);
            rule__Model__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__0"


    // $ANTLR start "rule__Model__Group__0__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:685:1: rule__Model__Group__0__Impl : ( () ) ;
    public final void rule__Model__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:689:1: ( ( () ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:690:1: ( () )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:690:1: ( () )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:691:1: ()
            {
             before(grammarAccess.getModelAccess().getModelAction_0()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:692:1: ()
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:694:1: 
            {
            }

             after(grammarAccess.getModelAccess().getModelAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__0__Impl"


    // $ANTLR start "rule__Model__Group__1"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:704:1: rule__Model__Group__1 : rule__Model__Group__1__Impl ;
    public final void rule__Model__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:708:1: ( rule__Model__Group__1__Impl )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:709:2: rule__Model__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Model__Group__1__Impl_in_rule__Model__Group__11534);
            rule__Model__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__1"


    // $ANTLR start "rule__Model__Group__1__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:715:1: rule__Model__Group__1__Impl : ( ( rule__Model__Group_1__0 )? ) ;
    public final void rule__Model__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:719:1: ( ( ( rule__Model__Group_1__0 )? ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:720:1: ( ( rule__Model__Group_1__0 )? )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:720:1: ( ( rule__Model__Group_1__0 )? )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:721:1: ( rule__Model__Group_1__0 )?
            {
             before(grammarAccess.getModelAccess().getGroup_1()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:722:1: ( rule__Model__Group_1__0 )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==RULE_ID||(LA9_0>=11 && LA9_0<=17)) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:722:2: rule__Model__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Model__Group_1__0_in_rule__Model__Group__1__Impl1561);
                    rule__Model__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getModelAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__1__Impl"


    // $ANTLR start "rule__Model__Group_1__0"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:736:1: rule__Model__Group_1__0 : rule__Model__Group_1__0__Impl rule__Model__Group_1__1 ;
    public final void rule__Model__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:740:1: ( rule__Model__Group_1__0__Impl rule__Model__Group_1__1 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:741:2: rule__Model__Group_1__0__Impl rule__Model__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Model__Group_1__0__Impl_in_rule__Model__Group_1__01596);
            rule__Model__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Model__Group_1__1_in_rule__Model__Group_1__01599);
            rule__Model__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group_1__0"


    // $ANTLR start "rule__Model__Group_1__0__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:748:1: rule__Model__Group_1__0__Impl : ( ( rule__Model__RootAssignment_1_0 ) ) ;
    public final void rule__Model__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:752:1: ( ( ( rule__Model__RootAssignment_1_0 ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:753:1: ( ( rule__Model__RootAssignment_1_0 ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:753:1: ( ( rule__Model__RootAssignment_1_0 ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:754:1: ( rule__Model__RootAssignment_1_0 )
            {
             before(grammarAccess.getModelAccess().getRootAssignment_1_0()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:755:1: ( rule__Model__RootAssignment_1_0 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:755:2: rule__Model__RootAssignment_1_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Model__RootAssignment_1_0_in_rule__Model__Group_1__0__Impl1626);
            rule__Model__RootAssignment_1_0();

            state._fsp--;


            }

             after(grammarAccess.getModelAccess().getRootAssignment_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group_1__0__Impl"


    // $ANTLR start "rule__Model__Group_1__1"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:765:1: rule__Model__Group_1__1 : rule__Model__Group_1__1__Impl ;
    public final void rule__Model__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:769:1: ( rule__Model__Group_1__1__Impl )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:770:2: rule__Model__Group_1__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Model__Group_1__1__Impl_in_rule__Model__Group_1__11656);
            rule__Model__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group_1__1"


    // $ANTLR start "rule__Model__Group_1__1__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:776:1: rule__Model__Group_1__1__Impl : ( ( rule__Model__RootAssignment_1_1 )* ) ;
    public final void rule__Model__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:780:1: ( ( ( rule__Model__RootAssignment_1_1 )* ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:781:1: ( ( rule__Model__RootAssignment_1_1 )* )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:781:1: ( ( rule__Model__RootAssignment_1_1 )* )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:782:1: ( rule__Model__RootAssignment_1_1 )*
            {
             before(grammarAccess.getModelAccess().getRootAssignment_1_1()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:783:1: ( rule__Model__RootAssignment_1_1 )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==RULE_ID||(LA10_0>=11 && LA10_0<=17)) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:783:2: rule__Model__RootAssignment_1_1
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Model__RootAssignment_1_1_in_rule__Model__Group_1__1__Impl1683);
            	    rule__Model__RootAssignment_1_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

             after(grammarAccess.getModelAccess().getRootAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group_1__1__Impl"


    // $ANTLR start "rule__Dependency__Group__0"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:797:1: rule__Dependency__Group__0 : rule__Dependency__Group__0__Impl rule__Dependency__Group__1 ;
    public final void rule__Dependency__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:801:1: ( rule__Dependency__Group__0__Impl rule__Dependency__Group__1 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:802:2: rule__Dependency__Group__0__Impl rule__Dependency__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Dependency__Group__0__Impl_in_rule__Dependency__Group__01718);
            rule__Dependency__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Dependency__Group__1_in_rule__Dependency__Group__01721);
            rule__Dependency__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dependency__Group__0"


    // $ANTLR start "rule__Dependency__Group__0__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:809:1: rule__Dependency__Group__0__Impl : ( ruleTerm ) ;
    public final void rule__Dependency__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:813:1: ( ( ruleTerm ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:814:1: ( ruleTerm )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:814:1: ( ruleTerm )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:815:1: ruleTerm
            {
             before(grammarAccess.getDependencyAccess().getTermParserRuleCall_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleTerm_in_rule__Dependency__Group__0__Impl1748);
            ruleTerm();

            state._fsp--;

             after(grammarAccess.getDependencyAccess().getTermParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dependency__Group__0__Impl"


    // $ANTLR start "rule__Dependency__Group__1"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:826:1: rule__Dependency__Group__1 : rule__Dependency__Group__1__Impl ;
    public final void rule__Dependency__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:830:1: ( rule__Dependency__Group__1__Impl )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:831:2: rule__Dependency__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Dependency__Group__1__Impl_in_rule__Dependency__Group__11777);
            rule__Dependency__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dependency__Group__1"


    // $ANTLR start "rule__Dependency__Group__1__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:837:1: rule__Dependency__Group__1__Impl : ( ( rule__Dependency__Group_1__0 )* ) ;
    public final void rule__Dependency__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:841:1: ( ( ( rule__Dependency__Group_1__0 )* ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:842:1: ( ( rule__Dependency__Group_1__0 )* )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:842:1: ( ( rule__Dependency__Group_1__0 )* )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:843:1: ( rule__Dependency__Group_1__0 )*
            {
             before(grammarAccess.getDependencyAccess().getGroup_1()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:844:1: ( rule__Dependency__Group_1__0 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( ((LA11_0>=22 && LA11_0<=29)) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:844:2: rule__Dependency__Group_1__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Dependency__Group_1__0_in_rule__Dependency__Group__1__Impl1804);
            	    rule__Dependency__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

             after(grammarAccess.getDependencyAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dependency__Group__1__Impl"


    // $ANTLR start "rule__Dependency__Group_1__0"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:858:1: rule__Dependency__Group_1__0 : rule__Dependency__Group_1__0__Impl rule__Dependency__Group_1__1 ;
    public final void rule__Dependency__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:862:1: ( rule__Dependency__Group_1__0__Impl rule__Dependency__Group_1__1 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:863:2: rule__Dependency__Group_1__0__Impl rule__Dependency__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Dependency__Group_1__0__Impl_in_rule__Dependency__Group_1__01839);
            rule__Dependency__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Dependency__Group_1__1_in_rule__Dependency__Group_1__01842);
            rule__Dependency__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dependency__Group_1__0"


    // $ANTLR start "rule__Dependency__Group_1__0__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:870:1: rule__Dependency__Group_1__0__Impl : ( () ) ;
    public final void rule__Dependency__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:874:1: ( ( () ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:875:1: ( () )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:875:1: ( () )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:876:1: ()
            {
             before(grammarAccess.getDependencyAccess().getBiDepLeftDepAction_1_0()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:877:1: ()
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:879:1: 
            {
            }

             after(grammarAccess.getDependencyAccess().getBiDepLeftDepAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dependency__Group_1__0__Impl"


    // $ANTLR start "rule__Dependency__Group_1__1"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:889:1: rule__Dependency__Group_1__1 : rule__Dependency__Group_1__1__Impl rule__Dependency__Group_1__2 ;
    public final void rule__Dependency__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:893:1: ( rule__Dependency__Group_1__1__Impl rule__Dependency__Group_1__2 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:894:2: rule__Dependency__Group_1__1__Impl rule__Dependency__Group_1__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Dependency__Group_1__1__Impl_in_rule__Dependency__Group_1__11900);
            rule__Dependency__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Dependency__Group_1__2_in_rule__Dependency__Group_1__11903);
            rule__Dependency__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dependency__Group_1__1"


    // $ANTLR start "rule__Dependency__Group_1__1__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:901:1: rule__Dependency__Group_1__1__Impl : ( ( rule__Dependency__OpAssignment_1_1 ) ) ;
    public final void rule__Dependency__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:905:1: ( ( ( rule__Dependency__OpAssignment_1_1 ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:906:1: ( ( rule__Dependency__OpAssignment_1_1 ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:906:1: ( ( rule__Dependency__OpAssignment_1_1 ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:907:1: ( rule__Dependency__OpAssignment_1_1 )
            {
             before(grammarAccess.getDependencyAccess().getOpAssignment_1_1()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:908:1: ( rule__Dependency__OpAssignment_1_1 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:908:2: rule__Dependency__OpAssignment_1_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Dependency__OpAssignment_1_1_in_rule__Dependency__Group_1__1__Impl1930);
            rule__Dependency__OpAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getDependencyAccess().getOpAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dependency__Group_1__1__Impl"


    // $ANTLR start "rule__Dependency__Group_1__2"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:918:1: rule__Dependency__Group_1__2 : rule__Dependency__Group_1__2__Impl ;
    public final void rule__Dependency__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:922:1: ( rule__Dependency__Group_1__2__Impl )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:923:2: rule__Dependency__Group_1__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Dependency__Group_1__2__Impl_in_rule__Dependency__Group_1__21960);
            rule__Dependency__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dependency__Group_1__2"


    // $ANTLR start "rule__Dependency__Group_1__2__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:929:1: rule__Dependency__Group_1__2__Impl : ( ( rule__Dependency__RightDepAssignment_1_2 ) ) ;
    public final void rule__Dependency__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:933:1: ( ( ( rule__Dependency__RightDepAssignment_1_2 ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:934:1: ( ( rule__Dependency__RightDepAssignment_1_2 ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:934:1: ( ( rule__Dependency__RightDepAssignment_1_2 ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:935:1: ( rule__Dependency__RightDepAssignment_1_2 )
            {
             before(grammarAccess.getDependencyAccess().getRightDepAssignment_1_2()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:936:1: ( rule__Dependency__RightDepAssignment_1_2 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:936:2: rule__Dependency__RightDepAssignment_1_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Dependency__RightDepAssignment_1_2_in_rule__Dependency__Group_1__2__Impl1987);
            rule__Dependency__RightDepAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getDependencyAccess().getRightDepAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dependency__Group_1__2__Impl"


    // $ANTLR start "rule__Term__Group_0__0"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:952:1: rule__Term__Group_0__0 : rule__Term__Group_0__0__Impl rule__Term__Group_0__1 ;
    public final void rule__Term__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:956:1: ( rule__Term__Group_0__0__Impl rule__Term__Group_0__1 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:957:2: rule__Term__Group_0__0__Impl rule__Term__Group_0__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Term__Group_0__0__Impl_in_rule__Term__Group_0__02023);
            rule__Term__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Term__Group_0__1_in_rule__Term__Group_0__02026);
            rule__Term__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__Group_0__0"


    // $ANTLR start "rule__Term__Group_0__0__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:964:1: rule__Term__Group_0__0__Impl : ( '(' ) ;
    public final void rule__Term__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:968:1: ( ( '(' ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:969:1: ( '(' )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:969:1: ( '(' )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:970:1: '('
            {
             before(grammarAccess.getTermAccess().getLeftParenthesisKeyword_0_0()); 
            match(input,32,FollowSets000.FOLLOW_32_in_rule__Term__Group_0__0__Impl2054); 
             after(grammarAccess.getTermAccess().getLeftParenthesisKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__Group_0__0__Impl"


    // $ANTLR start "rule__Term__Group_0__1"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:983:1: rule__Term__Group_0__1 : rule__Term__Group_0__1__Impl rule__Term__Group_0__2 ;
    public final void rule__Term__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:987:1: ( rule__Term__Group_0__1__Impl rule__Term__Group_0__2 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:988:2: rule__Term__Group_0__1__Impl rule__Term__Group_0__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Term__Group_0__1__Impl_in_rule__Term__Group_0__12085);
            rule__Term__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Term__Group_0__2_in_rule__Term__Group_0__12088);
            rule__Term__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__Group_0__1"


    // $ANTLR start "rule__Term__Group_0__1__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:995:1: rule__Term__Group_0__1__Impl : ( ruleDependency ) ;
    public final void rule__Term__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:999:1: ( ( ruleDependency ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1000:1: ( ruleDependency )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1000:1: ( ruleDependency )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1001:1: ruleDependency
            {
             before(grammarAccess.getTermAccess().getDependencyParserRuleCall_0_1()); 
            pushFollow(FollowSets000.FOLLOW_ruleDependency_in_rule__Term__Group_0__1__Impl2115);
            ruleDependency();

            state._fsp--;

             after(grammarAccess.getTermAccess().getDependencyParserRuleCall_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__Group_0__1__Impl"


    // $ANTLR start "rule__Term__Group_0__2"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1012:1: rule__Term__Group_0__2 : rule__Term__Group_0__2__Impl ;
    public final void rule__Term__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1016:1: ( rule__Term__Group_0__2__Impl )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1017:2: rule__Term__Group_0__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Term__Group_0__2__Impl_in_rule__Term__Group_0__22144);
            rule__Term__Group_0__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__Group_0__2"


    // $ANTLR start "rule__Term__Group_0__2__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1023:1: rule__Term__Group_0__2__Impl : ( ')' ) ;
    public final void rule__Term__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1027:1: ( ( ')' ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1028:1: ( ')' )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1028:1: ( ')' )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1029:1: ')'
            {
             before(grammarAccess.getTermAccess().getRightParenthesisKeyword_0_2()); 
            match(input,33,FollowSets000.FOLLOW_33_in_rule__Term__Group_0__2__Impl2172); 
             after(grammarAccess.getTermAccess().getRightParenthesisKeyword_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Term__Group_0__2__Impl"


    // $ANTLR start "rule__Component__Group__0"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1048:1: rule__Component__Group__0 : rule__Component__Group__0__Impl rule__Component__Group__1 ;
    public final void rule__Component__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1052:1: ( rule__Component__Group__0__Impl rule__Component__Group__1 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1053:2: rule__Component__Group__0__Impl rule__Component__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Component__Group__0__Impl_in_rule__Component__Group__02209);
            rule__Component__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Component__Group__1_in_rule__Component__Group__02212);
            rule__Component__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group__0"


    // $ANTLR start "rule__Component__Group__0__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1060:1: rule__Component__Group__0__Impl : ( ( rule__Component__TypeAssignment_0 )? ) ;
    public final void rule__Component__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1064:1: ( ( ( rule__Component__TypeAssignment_0 )? ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1065:1: ( ( rule__Component__TypeAssignment_0 )? )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1065:1: ( ( rule__Component__TypeAssignment_0 )? )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1066:1: ( rule__Component__TypeAssignment_0 )?
            {
             before(grammarAccess.getComponentAccess().getTypeAssignment_0()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1067:1: ( rule__Component__TypeAssignment_0 )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( ((LA12_0>=11 && LA12_0<=17)) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1067:2: rule__Component__TypeAssignment_0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Component__TypeAssignment_0_in_rule__Component__Group__0__Impl2239);
                    rule__Component__TypeAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getComponentAccess().getTypeAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group__0__Impl"


    // $ANTLR start "rule__Component__Group__1"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1077:1: rule__Component__Group__1 : rule__Component__Group__1__Impl rule__Component__Group__2 ;
    public final void rule__Component__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1081:1: ( rule__Component__Group__1__Impl rule__Component__Group__2 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1082:2: rule__Component__Group__1__Impl rule__Component__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Component__Group__1__Impl_in_rule__Component__Group__12270);
            rule__Component__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Component__Group__2_in_rule__Component__Group__12273);
            rule__Component__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group__1"


    // $ANTLR start "rule__Component__Group__1__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1089:1: rule__Component__Group__1__Impl : ( ( rule__Component__NameAssignment_1 ) ) ;
    public final void rule__Component__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1093:1: ( ( ( rule__Component__NameAssignment_1 ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1094:1: ( ( rule__Component__NameAssignment_1 ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1094:1: ( ( rule__Component__NameAssignment_1 ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1095:1: ( rule__Component__NameAssignment_1 )
            {
             before(grammarAccess.getComponentAccess().getNameAssignment_1()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1096:1: ( rule__Component__NameAssignment_1 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1096:2: rule__Component__NameAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Component__NameAssignment_1_in_rule__Component__Group__1__Impl2300);
            rule__Component__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getComponentAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group__1__Impl"


    // $ANTLR start "rule__Component__Group__2"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1106:1: rule__Component__Group__2 : rule__Component__Group__2__Impl rule__Component__Group__3 ;
    public final void rule__Component__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1110:1: ( rule__Component__Group__2__Impl rule__Component__Group__3 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1111:2: rule__Component__Group__2__Impl rule__Component__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Component__Group__2__Impl_in_rule__Component__Group__22330);
            rule__Component__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Component__Group__3_in_rule__Component__Group__22333);
            rule__Component__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group__2"


    // $ANTLR start "rule__Component__Group__2__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1118:1: rule__Component__Group__2__Impl : ( ( rule__Component__Alternatives_2 ) ) ;
    public final void rule__Component__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1122:1: ( ( ( rule__Component__Alternatives_2 ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1123:1: ( ( rule__Component__Alternatives_2 ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1123:1: ( ( rule__Component__Alternatives_2 ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1124:1: ( rule__Component__Alternatives_2 )
            {
             before(grammarAccess.getComponentAccess().getAlternatives_2()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1125:1: ( rule__Component__Alternatives_2 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1125:2: rule__Component__Alternatives_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Component__Alternatives_2_in_rule__Component__Group__2__Impl2360);
            rule__Component__Alternatives_2();

            state._fsp--;


            }

             after(grammarAccess.getComponentAccess().getAlternatives_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group__2__Impl"


    // $ANTLR start "rule__Component__Group__3"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1135:1: rule__Component__Group__3 : rule__Component__Group__3__Impl ;
    public final void rule__Component__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1139:1: ( rule__Component__Group__3__Impl )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1140:2: rule__Component__Group__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Component__Group__3__Impl_in_rule__Component__Group__32390);
            rule__Component__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group__3"


    // $ANTLR start "rule__Component__Group__3__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1146:1: rule__Component__Group__3__Impl : ( ( rule__Component__Group_3__0 )? ) ;
    public final void rule__Component__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1150:1: ( ( ( rule__Component__Group_3__0 )? ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1151:1: ( ( rule__Component__Group_3__0 )? )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1151:1: ( ( rule__Component__Group_3__0 )? )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1152:1: ( rule__Component__Group_3__0 )?
            {
             before(grammarAccess.getComponentAccess().getGroup_3()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1153:1: ( rule__Component__Group_3__0 )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==38) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1153:2: rule__Component__Group_3__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Component__Group_3__0_in_rule__Component__Group__3__Impl2417);
                    rule__Component__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getComponentAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group__3__Impl"


    // $ANTLR start "rule__Component__Group_2_1__0"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1171:1: rule__Component__Group_2_1__0 : rule__Component__Group_2_1__0__Impl rule__Component__Group_2_1__1 ;
    public final void rule__Component__Group_2_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1175:1: ( rule__Component__Group_2_1__0__Impl rule__Component__Group_2_1__1 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1176:2: rule__Component__Group_2_1__0__Impl rule__Component__Group_2_1__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Component__Group_2_1__0__Impl_in_rule__Component__Group_2_1__02456);
            rule__Component__Group_2_1__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Component__Group_2_1__1_in_rule__Component__Group_2_1__02459);
            rule__Component__Group_2_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group_2_1__0"


    // $ANTLR start "rule__Component__Group_2_1__0__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1183:1: rule__Component__Group_2_1__0__Impl : ( 'availableOptions' ) ;
    public final void rule__Component__Group_2_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1187:1: ( ( 'availableOptions' ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1188:1: ( 'availableOptions' )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1188:1: ( 'availableOptions' )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1189:1: 'availableOptions'
            {
             before(grammarAccess.getComponentAccess().getAvailableOptionsKeyword_2_1_0()); 
            match(input,34,FollowSets000.FOLLOW_34_in_rule__Component__Group_2_1__0__Impl2487); 
             after(grammarAccess.getComponentAccess().getAvailableOptionsKeyword_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group_2_1__0__Impl"


    // $ANTLR start "rule__Component__Group_2_1__1"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1202:1: rule__Component__Group_2_1__1 : rule__Component__Group_2_1__1__Impl rule__Component__Group_2_1__2 ;
    public final void rule__Component__Group_2_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1206:1: ( rule__Component__Group_2_1__1__Impl rule__Component__Group_2_1__2 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1207:2: rule__Component__Group_2_1__1__Impl rule__Component__Group_2_1__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Component__Group_2_1__1__Impl_in_rule__Component__Group_2_1__12518);
            rule__Component__Group_2_1__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Component__Group_2_1__2_in_rule__Component__Group_2_1__12521);
            rule__Component__Group_2_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group_2_1__1"


    // $ANTLR start "rule__Component__Group_2_1__1__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1214:1: rule__Component__Group_2_1__1__Impl : ( '{' ) ;
    public final void rule__Component__Group_2_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1218:1: ( ( '{' ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1219:1: ( '{' )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1219:1: ( '{' )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1220:1: '{'
            {
             before(grammarAccess.getComponentAccess().getLeftCurlyBracketKeyword_2_1_1()); 
            match(input,35,FollowSets000.FOLLOW_35_in_rule__Component__Group_2_1__1__Impl2549); 
             after(grammarAccess.getComponentAccess().getLeftCurlyBracketKeyword_2_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group_2_1__1__Impl"


    // $ANTLR start "rule__Component__Group_2_1__2"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1233:1: rule__Component__Group_2_1__2 : rule__Component__Group_2_1__2__Impl rule__Component__Group_2_1__3 ;
    public final void rule__Component__Group_2_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1237:1: ( rule__Component__Group_2_1__2__Impl rule__Component__Group_2_1__3 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1238:2: rule__Component__Group_2_1__2__Impl rule__Component__Group_2_1__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Component__Group_2_1__2__Impl_in_rule__Component__Group_2_1__22580);
            rule__Component__Group_2_1__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Component__Group_2_1__3_in_rule__Component__Group_2_1__22583);
            rule__Component__Group_2_1__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group_2_1__2"


    // $ANTLR start "rule__Component__Group_2_1__2__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1245:1: rule__Component__Group_2_1__2__Impl : ( ( rule__Component__AvailableOptionsAssignment_2_1_2 ) ) ;
    public final void rule__Component__Group_2_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1249:1: ( ( ( rule__Component__AvailableOptionsAssignment_2_1_2 ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1250:1: ( ( rule__Component__AvailableOptionsAssignment_2_1_2 ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1250:1: ( ( rule__Component__AvailableOptionsAssignment_2_1_2 ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1251:1: ( rule__Component__AvailableOptionsAssignment_2_1_2 )
            {
             before(grammarAccess.getComponentAccess().getAvailableOptionsAssignment_2_1_2()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1252:1: ( rule__Component__AvailableOptionsAssignment_2_1_2 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1252:2: rule__Component__AvailableOptionsAssignment_2_1_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Component__AvailableOptionsAssignment_2_1_2_in_rule__Component__Group_2_1__2__Impl2610);
            rule__Component__AvailableOptionsAssignment_2_1_2();

            state._fsp--;


            }

             after(grammarAccess.getComponentAccess().getAvailableOptionsAssignment_2_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group_2_1__2__Impl"


    // $ANTLR start "rule__Component__Group_2_1__3"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1262:1: rule__Component__Group_2_1__3 : rule__Component__Group_2_1__3__Impl rule__Component__Group_2_1__4 ;
    public final void rule__Component__Group_2_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1266:1: ( rule__Component__Group_2_1__3__Impl rule__Component__Group_2_1__4 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1267:2: rule__Component__Group_2_1__3__Impl rule__Component__Group_2_1__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Component__Group_2_1__3__Impl_in_rule__Component__Group_2_1__32640);
            rule__Component__Group_2_1__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Component__Group_2_1__4_in_rule__Component__Group_2_1__32643);
            rule__Component__Group_2_1__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group_2_1__3"


    // $ANTLR start "rule__Component__Group_2_1__3__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1274:1: rule__Component__Group_2_1__3__Impl : ( ( rule__Component__Group_2_1_3__0 )* ) ;
    public final void rule__Component__Group_2_1__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1278:1: ( ( ( rule__Component__Group_2_1_3__0 )* ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1279:1: ( ( rule__Component__Group_2_1_3__0 )* )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1279:1: ( ( rule__Component__Group_2_1_3__0 )* )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1280:1: ( rule__Component__Group_2_1_3__0 )*
            {
             before(grammarAccess.getComponentAccess().getGroup_2_1_3()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1281:1: ( rule__Component__Group_2_1_3__0 )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==37) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1281:2: rule__Component__Group_2_1_3__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Component__Group_2_1_3__0_in_rule__Component__Group_2_1__3__Impl2670);
            	    rule__Component__Group_2_1_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

             after(grammarAccess.getComponentAccess().getGroup_2_1_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group_2_1__3__Impl"


    // $ANTLR start "rule__Component__Group_2_1__4"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1291:1: rule__Component__Group_2_1__4 : rule__Component__Group_2_1__4__Impl ;
    public final void rule__Component__Group_2_1__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1295:1: ( rule__Component__Group_2_1__4__Impl )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1296:2: rule__Component__Group_2_1__4__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Component__Group_2_1__4__Impl_in_rule__Component__Group_2_1__42701);
            rule__Component__Group_2_1__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group_2_1__4"


    // $ANTLR start "rule__Component__Group_2_1__4__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1302:1: rule__Component__Group_2_1__4__Impl : ( '}' ) ;
    public final void rule__Component__Group_2_1__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1306:1: ( ( '}' ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1307:1: ( '}' )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1307:1: ( '}' )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1308:1: '}'
            {
             before(grammarAccess.getComponentAccess().getRightCurlyBracketKeyword_2_1_4()); 
            match(input,36,FollowSets000.FOLLOW_36_in_rule__Component__Group_2_1__4__Impl2729); 
             after(grammarAccess.getComponentAccess().getRightCurlyBracketKeyword_2_1_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group_2_1__4__Impl"


    // $ANTLR start "rule__Component__Group_2_1_3__0"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1331:1: rule__Component__Group_2_1_3__0 : rule__Component__Group_2_1_3__0__Impl rule__Component__Group_2_1_3__1 ;
    public final void rule__Component__Group_2_1_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1335:1: ( rule__Component__Group_2_1_3__0__Impl rule__Component__Group_2_1_3__1 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1336:2: rule__Component__Group_2_1_3__0__Impl rule__Component__Group_2_1_3__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Component__Group_2_1_3__0__Impl_in_rule__Component__Group_2_1_3__02770);
            rule__Component__Group_2_1_3__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Component__Group_2_1_3__1_in_rule__Component__Group_2_1_3__02773);
            rule__Component__Group_2_1_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group_2_1_3__0"


    // $ANTLR start "rule__Component__Group_2_1_3__0__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1343:1: rule__Component__Group_2_1_3__0__Impl : ( ',' ) ;
    public final void rule__Component__Group_2_1_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1347:1: ( ( ',' ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1348:1: ( ',' )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1348:1: ( ',' )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1349:1: ','
            {
             before(grammarAccess.getComponentAccess().getCommaKeyword_2_1_3_0()); 
            match(input,37,FollowSets000.FOLLOW_37_in_rule__Component__Group_2_1_3__0__Impl2801); 
             after(grammarAccess.getComponentAccess().getCommaKeyword_2_1_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group_2_1_3__0__Impl"


    // $ANTLR start "rule__Component__Group_2_1_3__1"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1362:1: rule__Component__Group_2_1_3__1 : rule__Component__Group_2_1_3__1__Impl ;
    public final void rule__Component__Group_2_1_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1366:1: ( rule__Component__Group_2_1_3__1__Impl )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1367:2: rule__Component__Group_2_1_3__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Component__Group_2_1_3__1__Impl_in_rule__Component__Group_2_1_3__12832);
            rule__Component__Group_2_1_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group_2_1_3__1"


    // $ANTLR start "rule__Component__Group_2_1_3__1__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1373:1: rule__Component__Group_2_1_3__1__Impl : ( ( rule__Component__AvailableOptionsAssignment_2_1_3_1 ) ) ;
    public final void rule__Component__Group_2_1_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1377:1: ( ( ( rule__Component__AvailableOptionsAssignment_2_1_3_1 ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1378:1: ( ( rule__Component__AvailableOptionsAssignment_2_1_3_1 ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1378:1: ( ( rule__Component__AvailableOptionsAssignment_2_1_3_1 ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1379:1: ( rule__Component__AvailableOptionsAssignment_2_1_3_1 )
            {
             before(grammarAccess.getComponentAccess().getAvailableOptionsAssignment_2_1_3_1()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1380:1: ( rule__Component__AvailableOptionsAssignment_2_1_3_1 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1380:2: rule__Component__AvailableOptionsAssignment_2_1_3_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Component__AvailableOptionsAssignment_2_1_3_1_in_rule__Component__Group_2_1_3__1__Impl2859);
            rule__Component__AvailableOptionsAssignment_2_1_3_1();

            state._fsp--;


            }

             after(grammarAccess.getComponentAccess().getAvailableOptionsAssignment_2_1_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group_2_1_3__1__Impl"


    // $ANTLR start "rule__Component__Group_3__0"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1394:1: rule__Component__Group_3__0 : rule__Component__Group_3__0__Impl rule__Component__Group_3__1 ;
    public final void rule__Component__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1398:1: ( rule__Component__Group_3__0__Impl rule__Component__Group_3__1 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1399:2: rule__Component__Group_3__0__Impl rule__Component__Group_3__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Component__Group_3__0__Impl_in_rule__Component__Group_3__02893);
            rule__Component__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Component__Group_3__1_in_rule__Component__Group_3__02896);
            rule__Component__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group_3__0"


    // $ANTLR start "rule__Component__Group_3__0__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1406:1: rule__Component__Group_3__0__Impl : ( 'depends' ) ;
    public final void rule__Component__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1410:1: ( ( 'depends' ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1411:1: ( 'depends' )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1411:1: ( 'depends' )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1412:1: 'depends'
            {
             before(grammarAccess.getComponentAccess().getDependsKeyword_3_0()); 
            match(input,38,FollowSets000.FOLLOW_38_in_rule__Component__Group_3__0__Impl2924); 
             after(grammarAccess.getComponentAccess().getDependsKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group_3__0__Impl"


    // $ANTLR start "rule__Component__Group_3__1"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1425:1: rule__Component__Group_3__1 : rule__Component__Group_3__1__Impl rule__Component__Group_3__2 ;
    public final void rule__Component__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1429:1: ( rule__Component__Group_3__1__Impl rule__Component__Group_3__2 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1430:2: rule__Component__Group_3__1__Impl rule__Component__Group_3__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Component__Group_3__1__Impl_in_rule__Component__Group_3__12955);
            rule__Component__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Component__Group_3__2_in_rule__Component__Group_3__12958);
            rule__Component__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group_3__1"


    // $ANTLR start "rule__Component__Group_3__1__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1437:1: rule__Component__Group_3__1__Impl : ( '{' ) ;
    public final void rule__Component__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1441:1: ( ( '{' ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1442:1: ( '{' )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1442:1: ( '{' )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1443:1: '{'
            {
             before(grammarAccess.getComponentAccess().getLeftCurlyBracketKeyword_3_1()); 
            match(input,35,FollowSets000.FOLLOW_35_in_rule__Component__Group_3__1__Impl2986); 
             after(grammarAccess.getComponentAccess().getLeftCurlyBracketKeyword_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group_3__1__Impl"


    // $ANTLR start "rule__Component__Group_3__2"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1456:1: rule__Component__Group_3__2 : rule__Component__Group_3__2__Impl rule__Component__Group_3__3 ;
    public final void rule__Component__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1460:1: ( rule__Component__Group_3__2__Impl rule__Component__Group_3__3 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1461:2: rule__Component__Group_3__2__Impl rule__Component__Group_3__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Component__Group_3__2__Impl_in_rule__Component__Group_3__23017);
            rule__Component__Group_3__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Component__Group_3__3_in_rule__Component__Group_3__23020);
            rule__Component__Group_3__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group_3__2"


    // $ANTLR start "rule__Component__Group_3__2__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1468:1: rule__Component__Group_3__2__Impl : ( ( rule__Component__DependsOnAssignment_3_2 ) ) ;
    public final void rule__Component__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1472:1: ( ( ( rule__Component__DependsOnAssignment_3_2 ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1473:1: ( ( rule__Component__DependsOnAssignment_3_2 ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1473:1: ( ( rule__Component__DependsOnAssignment_3_2 ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1474:1: ( rule__Component__DependsOnAssignment_3_2 )
            {
             before(grammarAccess.getComponentAccess().getDependsOnAssignment_3_2()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1475:1: ( rule__Component__DependsOnAssignment_3_2 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1475:2: rule__Component__DependsOnAssignment_3_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Component__DependsOnAssignment_3_2_in_rule__Component__Group_3__2__Impl3047);
            rule__Component__DependsOnAssignment_3_2();

            state._fsp--;


            }

             after(grammarAccess.getComponentAccess().getDependsOnAssignment_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group_3__2__Impl"


    // $ANTLR start "rule__Component__Group_3__3"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1485:1: rule__Component__Group_3__3 : rule__Component__Group_3__3__Impl rule__Component__Group_3__4 ;
    public final void rule__Component__Group_3__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1489:1: ( rule__Component__Group_3__3__Impl rule__Component__Group_3__4 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1490:2: rule__Component__Group_3__3__Impl rule__Component__Group_3__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Component__Group_3__3__Impl_in_rule__Component__Group_3__33077);
            rule__Component__Group_3__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Component__Group_3__4_in_rule__Component__Group_3__33080);
            rule__Component__Group_3__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group_3__3"


    // $ANTLR start "rule__Component__Group_3__3__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1497:1: rule__Component__Group_3__3__Impl : ( ( rule__Component__Group_3_3__0 )* ) ;
    public final void rule__Component__Group_3__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1501:1: ( ( ( rule__Component__Group_3_3__0 )* ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1502:1: ( ( rule__Component__Group_3_3__0 )* )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1502:1: ( ( rule__Component__Group_3_3__0 )* )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1503:1: ( rule__Component__Group_3_3__0 )*
            {
             before(grammarAccess.getComponentAccess().getGroup_3_3()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1504:1: ( rule__Component__Group_3_3__0 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==37) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1504:2: rule__Component__Group_3_3__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Component__Group_3_3__0_in_rule__Component__Group_3__3__Impl3107);
            	    rule__Component__Group_3_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

             after(grammarAccess.getComponentAccess().getGroup_3_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group_3__3__Impl"


    // $ANTLR start "rule__Component__Group_3__4"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1514:1: rule__Component__Group_3__4 : rule__Component__Group_3__4__Impl ;
    public final void rule__Component__Group_3__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1518:1: ( rule__Component__Group_3__4__Impl )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1519:2: rule__Component__Group_3__4__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Component__Group_3__4__Impl_in_rule__Component__Group_3__43138);
            rule__Component__Group_3__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group_3__4"


    // $ANTLR start "rule__Component__Group_3__4__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1525:1: rule__Component__Group_3__4__Impl : ( '}' ) ;
    public final void rule__Component__Group_3__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1529:1: ( ( '}' ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1530:1: ( '}' )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1530:1: ( '}' )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1531:1: '}'
            {
             before(grammarAccess.getComponentAccess().getRightCurlyBracketKeyword_3_4()); 
            match(input,36,FollowSets000.FOLLOW_36_in_rule__Component__Group_3__4__Impl3166); 
             after(grammarAccess.getComponentAccess().getRightCurlyBracketKeyword_3_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group_3__4__Impl"


    // $ANTLR start "rule__Component__Group_3_3__0"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1554:1: rule__Component__Group_3_3__0 : rule__Component__Group_3_3__0__Impl rule__Component__Group_3_3__1 ;
    public final void rule__Component__Group_3_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1558:1: ( rule__Component__Group_3_3__0__Impl rule__Component__Group_3_3__1 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1559:2: rule__Component__Group_3_3__0__Impl rule__Component__Group_3_3__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Component__Group_3_3__0__Impl_in_rule__Component__Group_3_3__03207);
            rule__Component__Group_3_3__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Component__Group_3_3__1_in_rule__Component__Group_3_3__03210);
            rule__Component__Group_3_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group_3_3__0"


    // $ANTLR start "rule__Component__Group_3_3__0__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1566:1: rule__Component__Group_3_3__0__Impl : ( ',' ) ;
    public final void rule__Component__Group_3_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1570:1: ( ( ',' ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1571:1: ( ',' )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1571:1: ( ',' )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1572:1: ','
            {
             before(grammarAccess.getComponentAccess().getCommaKeyword_3_3_0()); 
            match(input,37,FollowSets000.FOLLOW_37_in_rule__Component__Group_3_3__0__Impl3238); 
             after(grammarAccess.getComponentAccess().getCommaKeyword_3_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group_3_3__0__Impl"


    // $ANTLR start "rule__Component__Group_3_3__1"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1585:1: rule__Component__Group_3_3__1 : rule__Component__Group_3_3__1__Impl ;
    public final void rule__Component__Group_3_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1589:1: ( rule__Component__Group_3_3__1__Impl )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1590:2: rule__Component__Group_3_3__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Component__Group_3_3__1__Impl_in_rule__Component__Group_3_3__13269);
            rule__Component__Group_3_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group_3_3__1"


    // $ANTLR start "rule__Component__Group_3_3__1__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1596:1: rule__Component__Group_3_3__1__Impl : ( ( rule__Component__DependsOnAssignment_3_3_1 ) ) ;
    public final void rule__Component__Group_3_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1600:1: ( ( ( rule__Component__DependsOnAssignment_3_3_1 ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1601:1: ( ( rule__Component__DependsOnAssignment_3_3_1 ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1601:1: ( ( rule__Component__DependsOnAssignment_3_3_1 ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1602:1: ( rule__Component__DependsOnAssignment_3_3_1 )
            {
             before(grammarAccess.getComponentAccess().getDependsOnAssignment_3_3_1()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1603:1: ( rule__Component__DependsOnAssignment_3_3_1 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1603:2: rule__Component__DependsOnAssignment_3_3_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Component__DependsOnAssignment_3_3_1_in_rule__Component__Group_3_3__1__Impl3296);
            rule__Component__DependsOnAssignment_3_3_1();

            state._fsp--;


            }

             after(grammarAccess.getComponentAccess().getDependsOnAssignment_3_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group_3_3__1__Impl"


    // $ANTLR start "rule__Options__Group__0"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1617:1: rule__Options__Group__0 : rule__Options__Group__0__Impl rule__Options__Group__1 ;
    public final void rule__Options__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1621:1: ( rule__Options__Group__0__Impl rule__Options__Group__1 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1622:2: rule__Options__Group__0__Impl rule__Options__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Options__Group__0__Impl_in_rule__Options__Group__03330);
            rule__Options__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Options__Group__1_in_rule__Options__Group__03333);
            rule__Options__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Options__Group__0"


    // $ANTLR start "rule__Options__Group__0__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1629:1: rule__Options__Group__0__Impl : ( 'option' ) ;
    public final void rule__Options__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1633:1: ( ( 'option' ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1634:1: ( 'option' )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1634:1: ( 'option' )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1635:1: 'option'
            {
             before(grammarAccess.getOptionsAccess().getOptionKeyword_0()); 
            match(input,39,FollowSets000.FOLLOW_39_in_rule__Options__Group__0__Impl3361); 
             after(grammarAccess.getOptionsAccess().getOptionKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Options__Group__0__Impl"


    // $ANTLR start "rule__Options__Group__1"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1648:1: rule__Options__Group__1 : rule__Options__Group__1__Impl rule__Options__Group__2 ;
    public final void rule__Options__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1652:1: ( rule__Options__Group__1__Impl rule__Options__Group__2 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1653:2: rule__Options__Group__1__Impl rule__Options__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Options__Group__1__Impl_in_rule__Options__Group__13392);
            rule__Options__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Options__Group__2_in_rule__Options__Group__13395);
            rule__Options__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Options__Group__1"


    // $ANTLR start "rule__Options__Group__1__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1660:1: rule__Options__Group__1__Impl : ( ( rule__Options__NameAssignment_1 ) ) ;
    public final void rule__Options__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1664:1: ( ( ( rule__Options__NameAssignment_1 ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1665:1: ( ( rule__Options__NameAssignment_1 ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1665:1: ( ( rule__Options__NameAssignment_1 ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1666:1: ( rule__Options__NameAssignment_1 )
            {
             before(grammarAccess.getOptionsAccess().getNameAssignment_1()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1667:1: ( rule__Options__NameAssignment_1 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1667:2: rule__Options__NameAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Options__NameAssignment_1_in_rule__Options__Group__1__Impl3422);
            rule__Options__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getOptionsAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Options__Group__1__Impl"


    // $ANTLR start "rule__Options__Group__2"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1677:1: rule__Options__Group__2 : rule__Options__Group__2__Impl rule__Options__Group__3 ;
    public final void rule__Options__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1681:1: ( rule__Options__Group__2__Impl rule__Options__Group__3 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1682:2: rule__Options__Group__2__Impl rule__Options__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Options__Group__2__Impl_in_rule__Options__Group__23452);
            rule__Options__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Options__Group__3_in_rule__Options__Group__23455);
            rule__Options__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Options__Group__2"


    // $ANTLR start "rule__Options__Group__2__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1689:1: rule__Options__Group__2__Impl : ( ( rule__Options__DisplayTypeAssignment_2 )? ) ;
    public final void rule__Options__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1693:1: ( ( ( rule__Options__DisplayTypeAssignment_2 )? ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1694:1: ( ( rule__Options__DisplayTypeAssignment_2 )? )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1694:1: ( ( rule__Options__DisplayTypeAssignment_2 )? )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1695:1: ( rule__Options__DisplayTypeAssignment_2 )?
            {
             before(grammarAccess.getOptionsAccess().getDisplayTypeAssignment_2()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1696:1: ( rule__Options__DisplayTypeAssignment_2 )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( ((LA16_0>=18 && LA16_0<=21)) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1696:2: rule__Options__DisplayTypeAssignment_2
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Options__DisplayTypeAssignment_2_in_rule__Options__Group__2__Impl3482);
                    rule__Options__DisplayTypeAssignment_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getOptionsAccess().getDisplayTypeAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Options__Group__2__Impl"


    // $ANTLR start "rule__Options__Group__3"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1706:1: rule__Options__Group__3 : rule__Options__Group__3__Impl rule__Options__Group__4 ;
    public final void rule__Options__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1710:1: ( rule__Options__Group__3__Impl rule__Options__Group__4 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1711:2: rule__Options__Group__3__Impl rule__Options__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Options__Group__3__Impl_in_rule__Options__Group__33513);
            rule__Options__Group__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Options__Group__4_in_rule__Options__Group__33516);
            rule__Options__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Options__Group__3"


    // $ANTLR start "rule__Options__Group__3__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1718:1: rule__Options__Group__3__Impl : ( '{' ) ;
    public final void rule__Options__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1722:1: ( ( '{' ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1723:1: ( '{' )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1723:1: ( '{' )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1724:1: '{'
            {
             before(grammarAccess.getOptionsAccess().getLeftCurlyBracketKeyword_3()); 
            match(input,35,FollowSets000.FOLLOW_35_in_rule__Options__Group__3__Impl3544); 
             after(grammarAccess.getOptionsAccess().getLeftCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Options__Group__3__Impl"


    // $ANTLR start "rule__Options__Group__4"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1737:1: rule__Options__Group__4 : rule__Options__Group__4__Impl rule__Options__Group__5 ;
    public final void rule__Options__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1741:1: ( rule__Options__Group__4__Impl rule__Options__Group__5 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1742:2: rule__Options__Group__4__Impl rule__Options__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_rule__Options__Group__4__Impl_in_rule__Options__Group__43575);
            rule__Options__Group__4__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Options__Group__5_in_rule__Options__Group__43578);
            rule__Options__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Options__Group__4"


    // $ANTLR start "rule__Options__Group__4__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1749:1: rule__Options__Group__4__Impl : ( ( rule__Options__ComponentsAssignment_4 ) ) ;
    public final void rule__Options__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1753:1: ( ( ( rule__Options__ComponentsAssignment_4 ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1754:1: ( ( rule__Options__ComponentsAssignment_4 ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1754:1: ( ( rule__Options__ComponentsAssignment_4 ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1755:1: ( rule__Options__ComponentsAssignment_4 )
            {
             before(grammarAccess.getOptionsAccess().getComponentsAssignment_4()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1756:1: ( rule__Options__ComponentsAssignment_4 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1756:2: rule__Options__ComponentsAssignment_4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Options__ComponentsAssignment_4_in_rule__Options__Group__4__Impl3605);
            rule__Options__ComponentsAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getOptionsAccess().getComponentsAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Options__Group__4__Impl"


    // $ANTLR start "rule__Options__Group__5"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1766:1: rule__Options__Group__5 : rule__Options__Group__5__Impl rule__Options__Group__6 ;
    public final void rule__Options__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1770:1: ( rule__Options__Group__5__Impl rule__Options__Group__6 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1771:2: rule__Options__Group__5__Impl rule__Options__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_rule__Options__Group__5__Impl_in_rule__Options__Group__53635);
            rule__Options__Group__5__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Options__Group__6_in_rule__Options__Group__53638);
            rule__Options__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Options__Group__5"


    // $ANTLR start "rule__Options__Group__5__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1778:1: rule__Options__Group__5__Impl : ( ( rule__Options__Group_5__0 )* ) ;
    public final void rule__Options__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1782:1: ( ( ( rule__Options__Group_5__0 )* ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1783:1: ( ( rule__Options__Group_5__0 )* )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1783:1: ( ( rule__Options__Group_5__0 )* )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1784:1: ( rule__Options__Group_5__0 )*
            {
             before(grammarAccess.getOptionsAccess().getGroup_5()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1785:1: ( rule__Options__Group_5__0 )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==37) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1785:2: rule__Options__Group_5__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Options__Group_5__0_in_rule__Options__Group__5__Impl3665);
            	    rule__Options__Group_5__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

             after(grammarAccess.getOptionsAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Options__Group__5__Impl"


    // $ANTLR start "rule__Options__Group__6"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1795:1: rule__Options__Group__6 : rule__Options__Group__6__Impl ;
    public final void rule__Options__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1799:1: ( rule__Options__Group__6__Impl )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1800:2: rule__Options__Group__6__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Options__Group__6__Impl_in_rule__Options__Group__63696);
            rule__Options__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Options__Group__6"


    // $ANTLR start "rule__Options__Group__6__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1806:1: rule__Options__Group__6__Impl : ( '}' ) ;
    public final void rule__Options__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1810:1: ( ( '}' ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1811:1: ( '}' )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1811:1: ( '}' )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1812:1: '}'
            {
             before(grammarAccess.getOptionsAccess().getRightCurlyBracketKeyword_6()); 
            match(input,36,FollowSets000.FOLLOW_36_in_rule__Options__Group__6__Impl3724); 
             after(grammarAccess.getOptionsAccess().getRightCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Options__Group__6__Impl"


    // $ANTLR start "rule__Options__Group_5__0"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1839:1: rule__Options__Group_5__0 : rule__Options__Group_5__0__Impl rule__Options__Group_5__1 ;
    public final void rule__Options__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1843:1: ( rule__Options__Group_5__0__Impl rule__Options__Group_5__1 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1844:2: rule__Options__Group_5__0__Impl rule__Options__Group_5__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Options__Group_5__0__Impl_in_rule__Options__Group_5__03769);
            rule__Options__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Options__Group_5__1_in_rule__Options__Group_5__03772);
            rule__Options__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Options__Group_5__0"


    // $ANTLR start "rule__Options__Group_5__0__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1851:1: rule__Options__Group_5__0__Impl : ( ',' ) ;
    public final void rule__Options__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1855:1: ( ( ',' ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1856:1: ( ',' )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1856:1: ( ',' )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1857:1: ','
            {
             before(grammarAccess.getOptionsAccess().getCommaKeyword_5_0()); 
            match(input,37,FollowSets000.FOLLOW_37_in_rule__Options__Group_5__0__Impl3800); 
             after(grammarAccess.getOptionsAccess().getCommaKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Options__Group_5__0__Impl"


    // $ANTLR start "rule__Options__Group_5__1"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1870:1: rule__Options__Group_5__1 : rule__Options__Group_5__1__Impl ;
    public final void rule__Options__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1874:1: ( rule__Options__Group_5__1__Impl )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1875:2: rule__Options__Group_5__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Options__Group_5__1__Impl_in_rule__Options__Group_5__13831);
            rule__Options__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Options__Group_5__1"


    // $ANTLR start "rule__Options__Group_5__1__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1881:1: rule__Options__Group_5__1__Impl : ( ( rule__Options__ComponentsAssignment_5_1 ) ) ;
    public final void rule__Options__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1885:1: ( ( ( rule__Options__ComponentsAssignment_5_1 ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1886:1: ( ( rule__Options__ComponentsAssignment_5_1 ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1886:1: ( ( rule__Options__ComponentsAssignment_5_1 ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1887:1: ( rule__Options__ComponentsAssignment_5_1 )
            {
             before(grammarAccess.getOptionsAccess().getComponentsAssignment_5_1()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1888:1: ( rule__Options__ComponentsAssignment_5_1 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1888:2: rule__Options__ComponentsAssignment_5_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Options__ComponentsAssignment_5_1_in_rule__Options__Group_5__1__Impl3858);
            rule__Options__ComponentsAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getOptionsAccess().getComponentsAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Options__Group_5__1__Impl"


    // $ANTLR start "rule__DependencyStatement__Group__0"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1902:1: rule__DependencyStatement__Group__0 : rule__DependencyStatement__Group__0__Impl rule__DependencyStatement__Group__1 ;
    public final void rule__DependencyStatement__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1906:1: ( rule__DependencyStatement__Group__0__Impl rule__DependencyStatement__Group__1 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1907:2: rule__DependencyStatement__Group__0__Impl rule__DependencyStatement__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__DependencyStatement__Group__0__Impl_in_rule__DependencyStatement__Group__03892);
            rule__DependencyStatement__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__DependencyStatement__Group__1_in_rule__DependencyStatement__Group__03895);
            rule__DependencyStatement__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DependencyStatement__Group__0"


    // $ANTLR start "rule__DependencyStatement__Group__0__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1914:1: rule__DependencyStatement__Group__0__Impl : ( ( rule__DependencyStatement__NameAssignment_0 ) ) ;
    public final void rule__DependencyStatement__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1918:1: ( ( ( rule__DependencyStatement__NameAssignment_0 ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1919:1: ( ( rule__DependencyStatement__NameAssignment_0 ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1919:1: ( ( rule__DependencyStatement__NameAssignment_0 ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1920:1: ( rule__DependencyStatement__NameAssignment_0 )
            {
             before(grammarAccess.getDependencyStatementAccess().getNameAssignment_0()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1921:1: ( rule__DependencyStatement__NameAssignment_0 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1921:2: rule__DependencyStatement__NameAssignment_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__DependencyStatement__NameAssignment_0_in_rule__DependencyStatement__Group__0__Impl3922);
            rule__DependencyStatement__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getDependencyStatementAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DependencyStatement__Group__0__Impl"


    // $ANTLR start "rule__DependencyStatement__Group__1"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1931:1: rule__DependencyStatement__Group__1 : rule__DependencyStatement__Group__1__Impl rule__DependencyStatement__Group__2 ;
    public final void rule__DependencyStatement__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1935:1: ( rule__DependencyStatement__Group__1__Impl rule__DependencyStatement__Group__2 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1936:2: rule__DependencyStatement__Group__1__Impl rule__DependencyStatement__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__DependencyStatement__Group__1__Impl_in_rule__DependencyStatement__Group__13952);
            rule__DependencyStatement__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__DependencyStatement__Group__2_in_rule__DependencyStatement__Group__13955);
            rule__DependencyStatement__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DependencyStatement__Group__1"


    // $ANTLR start "rule__DependencyStatement__Group__1__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1943:1: rule__DependencyStatement__Group__1__Impl : ( '(' ) ;
    public final void rule__DependencyStatement__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1947:1: ( ( '(' ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1948:1: ( '(' )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1948:1: ( '(' )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1949:1: '('
            {
             before(grammarAccess.getDependencyStatementAccess().getLeftParenthesisKeyword_1()); 
            match(input,32,FollowSets000.FOLLOW_32_in_rule__DependencyStatement__Group__1__Impl3983); 
             after(grammarAccess.getDependencyStatementAccess().getLeftParenthesisKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DependencyStatement__Group__1__Impl"


    // $ANTLR start "rule__DependencyStatement__Group__2"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1962:1: rule__DependencyStatement__Group__2 : rule__DependencyStatement__Group__2__Impl rule__DependencyStatement__Group__3 ;
    public final void rule__DependencyStatement__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1966:1: ( rule__DependencyStatement__Group__2__Impl rule__DependencyStatement__Group__3 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1967:2: rule__DependencyStatement__Group__2__Impl rule__DependencyStatement__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__DependencyStatement__Group__2__Impl_in_rule__DependencyStatement__Group__24014);
            rule__DependencyStatement__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__DependencyStatement__Group__3_in_rule__DependencyStatement__Group__24017);
            rule__DependencyStatement__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DependencyStatement__Group__2"


    // $ANTLR start "rule__DependencyStatement__Group__2__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1974:1: rule__DependencyStatement__Group__2__Impl : ( ( rule__DependencyStatement__DependenciesAssignment_2 ) ) ;
    public final void rule__DependencyStatement__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1978:1: ( ( ( rule__DependencyStatement__DependenciesAssignment_2 ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1979:1: ( ( rule__DependencyStatement__DependenciesAssignment_2 ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1979:1: ( ( rule__DependencyStatement__DependenciesAssignment_2 ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1980:1: ( rule__DependencyStatement__DependenciesAssignment_2 )
            {
             before(grammarAccess.getDependencyStatementAccess().getDependenciesAssignment_2()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1981:1: ( rule__DependencyStatement__DependenciesAssignment_2 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1981:2: rule__DependencyStatement__DependenciesAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__DependencyStatement__DependenciesAssignment_2_in_rule__DependencyStatement__Group__2__Impl4044);
            rule__DependencyStatement__DependenciesAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getDependencyStatementAccess().getDependenciesAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DependencyStatement__Group__2__Impl"


    // $ANTLR start "rule__DependencyStatement__Group__3"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1991:1: rule__DependencyStatement__Group__3 : rule__DependencyStatement__Group__3__Impl ;
    public final void rule__DependencyStatement__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1995:1: ( rule__DependencyStatement__Group__3__Impl )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:1996:2: rule__DependencyStatement__Group__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__DependencyStatement__Group__3__Impl_in_rule__DependencyStatement__Group__34074);
            rule__DependencyStatement__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DependencyStatement__Group__3"


    // $ANTLR start "rule__DependencyStatement__Group__3__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2002:1: rule__DependencyStatement__Group__3__Impl : ( ')' ) ;
    public final void rule__DependencyStatement__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2006:1: ( ( ')' ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2007:1: ( ')' )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2007:1: ( ')' )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2008:1: ')'
            {
             before(grammarAccess.getDependencyStatementAccess().getRightParenthesisKeyword_3()); 
            match(input,33,FollowSets000.FOLLOW_33_in_rule__DependencyStatement__Group__3__Impl4102); 
             after(grammarAccess.getDependencyStatementAccess().getRightParenthesisKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DependencyStatement__Group__3__Impl"


    // $ANTLR start "rule__UnDep__Group__0"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2030:1: rule__UnDep__Group__0 : rule__UnDep__Group__0__Impl rule__UnDep__Group__1 ;
    public final void rule__UnDep__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2034:1: ( rule__UnDep__Group__0__Impl rule__UnDep__Group__1 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2035:2: rule__UnDep__Group__0__Impl rule__UnDep__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__UnDep__Group__0__Impl_in_rule__UnDep__Group__04142);
            rule__UnDep__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__UnDep__Group__1_in_rule__UnDep__Group__04145);
            rule__UnDep__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnDep__Group__0"


    // $ANTLR start "rule__UnDep__Group__0__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2042:1: rule__UnDep__Group__0__Impl : ( ( rule__UnDep__OpAssignment_0 ) ) ;
    public final void rule__UnDep__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2046:1: ( ( ( rule__UnDep__OpAssignment_0 ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2047:1: ( ( rule__UnDep__OpAssignment_0 ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2047:1: ( ( rule__UnDep__OpAssignment_0 ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2048:1: ( rule__UnDep__OpAssignment_0 )
            {
             before(grammarAccess.getUnDepAccess().getOpAssignment_0()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2049:1: ( rule__UnDep__OpAssignment_0 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2049:2: rule__UnDep__OpAssignment_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__UnDep__OpAssignment_0_in_rule__UnDep__Group__0__Impl4172);
            rule__UnDep__OpAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getUnDepAccess().getOpAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnDep__Group__0__Impl"


    // $ANTLR start "rule__UnDep__Group__1"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2059:1: rule__UnDep__Group__1 : rule__UnDep__Group__1__Impl ;
    public final void rule__UnDep__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2063:1: ( rule__UnDep__Group__1__Impl )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2064:2: rule__UnDep__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__UnDep__Group__1__Impl_in_rule__UnDep__Group__14202);
            rule__UnDep__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnDep__Group__1"


    // $ANTLR start "rule__UnDep__Group__1__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2070:1: rule__UnDep__Group__1__Impl : ( ( rule__UnDep__DepAssignment_1 ) ) ;
    public final void rule__UnDep__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2074:1: ( ( ( rule__UnDep__DepAssignment_1 ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2075:1: ( ( rule__UnDep__DepAssignment_1 ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2075:1: ( ( rule__UnDep__DepAssignment_1 ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2076:1: ( rule__UnDep__DepAssignment_1 )
            {
             before(grammarAccess.getUnDepAccess().getDepAssignment_1()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2077:1: ( rule__UnDep__DepAssignment_1 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2077:2: rule__UnDep__DepAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__UnDep__DepAssignment_1_in_rule__UnDep__Group__1__Impl4229);
            rule__UnDep__DepAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getUnDepAccess().getDepAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnDep__Group__1__Impl"


    // $ANTLR start "rule__Const__Group_0__0"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2091:1: rule__Const__Group_0__0 : rule__Const__Group_0__0__Impl rule__Const__Group_0__1 ;
    public final void rule__Const__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2095:1: ( rule__Const__Group_0__0__Impl rule__Const__Group_0__1 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2096:2: rule__Const__Group_0__0__Impl rule__Const__Group_0__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Const__Group_0__0__Impl_in_rule__Const__Group_0__04263);
            rule__Const__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Const__Group_0__1_in_rule__Const__Group_0__04266);
            rule__Const__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Const__Group_0__0"


    // $ANTLR start "rule__Const__Group_0__0__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2103:1: rule__Const__Group_0__0__Impl : ( () ) ;
    public final void rule__Const__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2107:1: ( ( () ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2108:1: ( () )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2108:1: ( () )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2109:1: ()
            {
             before(grammarAccess.getConstAccess().getConstAction_0_0()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2110:1: ()
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2112:1: 
            {
            }

             after(grammarAccess.getConstAccess().getConstAction_0_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Const__Group_0__0__Impl"


    // $ANTLR start "rule__Const__Group_0__1"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2122:1: rule__Const__Group_0__1 : rule__Const__Group_0__1__Impl ;
    public final void rule__Const__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2126:1: ( rule__Const__Group_0__1__Impl )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2127:2: rule__Const__Group_0__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Const__Group_0__1__Impl_in_rule__Const__Group_0__14324);
            rule__Const__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Const__Group_0__1"


    // $ANTLR start "rule__Const__Group_0__1__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2133:1: rule__Const__Group_0__1__Impl : ( ( rule__Const__Group_0_1__0 ) ) ;
    public final void rule__Const__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2137:1: ( ( ( rule__Const__Group_0_1__0 ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2138:1: ( ( rule__Const__Group_0_1__0 ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2138:1: ( ( rule__Const__Group_0_1__0 ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2139:1: ( rule__Const__Group_0_1__0 )
            {
             before(grammarAccess.getConstAccess().getGroup_0_1()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2140:1: ( rule__Const__Group_0_1__0 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2140:2: rule__Const__Group_0_1__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Const__Group_0_1__0_in_rule__Const__Group_0__1__Impl4351);
            rule__Const__Group_0_1__0();

            state._fsp--;


            }

             after(grammarAccess.getConstAccess().getGroup_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Const__Group_0__1__Impl"


    // $ANTLR start "rule__Const__Group_0_1__0"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2154:1: rule__Const__Group_0_1__0 : rule__Const__Group_0_1__0__Impl rule__Const__Group_0_1__1 ;
    public final void rule__Const__Group_0_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2158:1: ( rule__Const__Group_0_1__0__Impl rule__Const__Group_0_1__1 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2159:2: rule__Const__Group_0_1__0__Impl rule__Const__Group_0_1__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Const__Group_0_1__0__Impl_in_rule__Const__Group_0_1__04385);
            rule__Const__Group_0_1__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Const__Group_0_1__1_in_rule__Const__Group_0_1__04388);
            rule__Const__Group_0_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Const__Group_0_1__0"


    // $ANTLR start "rule__Const__Group_0_1__0__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2166:1: rule__Const__Group_0_1__0__Impl : ( ( rule__Const__TypeAssignment_0_1_0 ) ) ;
    public final void rule__Const__Group_0_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2170:1: ( ( ( rule__Const__TypeAssignment_0_1_0 ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2171:1: ( ( rule__Const__TypeAssignment_0_1_0 ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2171:1: ( ( rule__Const__TypeAssignment_0_1_0 ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2172:1: ( rule__Const__TypeAssignment_0_1_0 )
            {
             before(grammarAccess.getConstAccess().getTypeAssignment_0_1_0()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2173:1: ( rule__Const__TypeAssignment_0_1_0 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2173:2: rule__Const__TypeAssignment_0_1_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Const__TypeAssignment_0_1_0_in_rule__Const__Group_0_1__0__Impl4415);
            rule__Const__TypeAssignment_0_1_0();

            state._fsp--;


            }

             after(grammarAccess.getConstAccess().getTypeAssignment_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Const__Group_0_1__0__Impl"


    // $ANTLR start "rule__Const__Group_0_1__1"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2183:1: rule__Const__Group_0_1__1 : rule__Const__Group_0_1__1__Impl ;
    public final void rule__Const__Group_0_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2187:1: ( rule__Const__Group_0_1__1__Impl )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2188:2: rule__Const__Group_0_1__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Const__Group_0_1__1__Impl_in_rule__Const__Group_0_1__14445);
            rule__Const__Group_0_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Const__Group_0_1__1"


    // $ANTLR start "rule__Const__Group_0_1__1__Impl"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2194:1: rule__Const__Group_0_1__1__Impl : ( ( rule__Const__ValueAssignment_0_1_1 ) ) ;
    public final void rule__Const__Group_0_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2198:1: ( ( ( rule__Const__ValueAssignment_0_1_1 ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2199:1: ( ( rule__Const__ValueAssignment_0_1_1 ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2199:1: ( ( rule__Const__ValueAssignment_0_1_1 ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2200:1: ( rule__Const__ValueAssignment_0_1_1 )
            {
             before(grammarAccess.getConstAccess().getValueAssignment_0_1_1()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2201:1: ( rule__Const__ValueAssignment_0_1_1 )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2201:2: rule__Const__ValueAssignment_0_1_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Const__ValueAssignment_0_1_1_in_rule__Const__Group_0_1__1__Impl4472);
            rule__Const__ValueAssignment_0_1_1();

            state._fsp--;


            }

             after(grammarAccess.getConstAccess().getValueAssignment_0_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Const__Group_0_1__1__Impl"


    // $ANTLR start "rule__Model__RootAssignment_1_0"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2216:1: rule__Model__RootAssignment_1_0 : ( ruleComponent ) ;
    public final void rule__Model__RootAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2220:1: ( ( ruleComponent ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2221:1: ( ruleComponent )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2221:1: ( ruleComponent )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2222:1: ruleComponent
            {
             before(grammarAccess.getModelAccess().getRootComponentParserRuleCall_1_0_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleComponent_in_rule__Model__RootAssignment_1_04511);
            ruleComponent();

            state._fsp--;

             after(grammarAccess.getModelAccess().getRootComponentParserRuleCall_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__RootAssignment_1_0"


    // $ANTLR start "rule__Model__RootAssignment_1_1"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2231:1: rule__Model__RootAssignment_1_1 : ( ruleComponent ) ;
    public final void rule__Model__RootAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2235:1: ( ( ruleComponent ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2236:1: ( ruleComponent )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2236:1: ( ruleComponent )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2237:1: ruleComponent
            {
             before(grammarAccess.getModelAccess().getRootComponentParserRuleCall_1_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleComponent_in_rule__Model__RootAssignment_1_14542);
            ruleComponent();

            state._fsp--;

             after(grammarAccess.getModelAccess().getRootComponentParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__RootAssignment_1_1"


    // $ANTLR start "rule__Dependency__OpAssignment_1_1"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2246:1: rule__Dependency__OpAssignment_1_1 : ( ruleBinOperator ) ;
    public final void rule__Dependency__OpAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2250:1: ( ( ruleBinOperator ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2251:1: ( ruleBinOperator )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2251:1: ( ruleBinOperator )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2252:1: ruleBinOperator
            {
             before(grammarAccess.getDependencyAccess().getOpBinOperatorEnumRuleCall_1_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleBinOperator_in_rule__Dependency__OpAssignment_1_14573);
            ruleBinOperator();

            state._fsp--;

             after(grammarAccess.getDependencyAccess().getOpBinOperatorEnumRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dependency__OpAssignment_1_1"


    // $ANTLR start "rule__Dependency__RightDepAssignment_1_2"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2261:1: rule__Dependency__RightDepAssignment_1_2 : ( ruleTerm ) ;
    public final void rule__Dependency__RightDepAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2265:1: ( ( ruleTerm ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2266:1: ( ruleTerm )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2266:1: ( ruleTerm )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2267:1: ruleTerm
            {
             before(grammarAccess.getDependencyAccess().getRightDepTermParserRuleCall_1_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleTerm_in_rule__Dependency__RightDepAssignment_1_24604);
            ruleTerm();

            state._fsp--;

             after(grammarAccess.getDependencyAccess().getRightDepTermParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dependency__RightDepAssignment_1_2"


    // $ANTLR start "rule__Component__TypeAssignment_0"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2276:1: rule__Component__TypeAssignment_0 : ( ruleSimpleTypes ) ;
    public final void rule__Component__TypeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2280:1: ( ( ruleSimpleTypes ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2281:1: ( ruleSimpleTypes )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2281:1: ( ruleSimpleTypes )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2282:1: ruleSimpleTypes
            {
             before(grammarAccess.getComponentAccess().getTypeSimpleTypesEnumRuleCall_0_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleSimpleTypes_in_rule__Component__TypeAssignment_04635);
            ruleSimpleTypes();

            state._fsp--;

             after(grammarAccess.getComponentAccess().getTypeSimpleTypesEnumRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__TypeAssignment_0"


    // $ANTLR start "rule__Component__NameAssignment_1"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2291:1: rule__Component__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Component__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2295:1: ( ( RULE_ID ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2296:1: ( RULE_ID )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2296:1: ( RULE_ID )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2297:1: RULE_ID
            {
             before(grammarAccess.getComponentAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__Component__NameAssignment_14666); 
             after(grammarAccess.getComponentAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__NameAssignment_1"


    // $ANTLR start "rule__Component__ValueAssignment_2_0"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2306:1: rule__Component__ValueAssignment_2_0 : ( ruleEString ) ;
    public final void rule__Component__ValueAssignment_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2310:1: ( ( ruleEString ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2311:1: ( ruleEString )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2311:1: ( ruleEString )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2312:1: ruleEString
            {
             before(grammarAccess.getComponentAccess().getValueEStringParserRuleCall_2_0_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Component__ValueAssignment_2_04697);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getComponentAccess().getValueEStringParserRuleCall_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__ValueAssignment_2_0"


    // $ANTLR start "rule__Component__AvailableOptionsAssignment_2_1_2"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2321:1: rule__Component__AvailableOptionsAssignment_2_1_2 : ( ruleOptions ) ;
    public final void rule__Component__AvailableOptionsAssignment_2_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2325:1: ( ( ruleOptions ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2326:1: ( ruleOptions )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2326:1: ( ruleOptions )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2327:1: ruleOptions
            {
             before(grammarAccess.getComponentAccess().getAvailableOptionsOptionsParserRuleCall_2_1_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleOptions_in_rule__Component__AvailableOptionsAssignment_2_1_24728);
            ruleOptions();

            state._fsp--;

             after(grammarAccess.getComponentAccess().getAvailableOptionsOptionsParserRuleCall_2_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__AvailableOptionsAssignment_2_1_2"


    // $ANTLR start "rule__Component__AvailableOptionsAssignment_2_1_3_1"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2336:1: rule__Component__AvailableOptionsAssignment_2_1_3_1 : ( ruleOptions ) ;
    public final void rule__Component__AvailableOptionsAssignment_2_1_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2340:1: ( ( ruleOptions ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2341:1: ( ruleOptions )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2341:1: ( ruleOptions )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2342:1: ruleOptions
            {
             before(grammarAccess.getComponentAccess().getAvailableOptionsOptionsParserRuleCall_2_1_3_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleOptions_in_rule__Component__AvailableOptionsAssignment_2_1_3_14759);
            ruleOptions();

            state._fsp--;

             after(grammarAccess.getComponentAccess().getAvailableOptionsOptionsParserRuleCall_2_1_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__AvailableOptionsAssignment_2_1_3_1"


    // $ANTLR start "rule__Component__DependsOnAssignment_3_2"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2351:1: rule__Component__DependsOnAssignment_3_2 : ( ruleDependencyStatement ) ;
    public final void rule__Component__DependsOnAssignment_3_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2355:1: ( ( ruleDependencyStatement ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2356:1: ( ruleDependencyStatement )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2356:1: ( ruleDependencyStatement )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2357:1: ruleDependencyStatement
            {
             before(grammarAccess.getComponentAccess().getDependsOnDependencyStatementParserRuleCall_3_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleDependencyStatement_in_rule__Component__DependsOnAssignment_3_24790);
            ruleDependencyStatement();

            state._fsp--;

             after(grammarAccess.getComponentAccess().getDependsOnDependencyStatementParserRuleCall_3_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__DependsOnAssignment_3_2"


    // $ANTLR start "rule__Component__DependsOnAssignment_3_3_1"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2366:1: rule__Component__DependsOnAssignment_3_3_1 : ( ruleDependencyStatement ) ;
    public final void rule__Component__DependsOnAssignment_3_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2370:1: ( ( ruleDependencyStatement ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2371:1: ( ruleDependencyStatement )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2371:1: ( ruleDependencyStatement )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2372:1: ruleDependencyStatement
            {
             before(grammarAccess.getComponentAccess().getDependsOnDependencyStatementParserRuleCall_3_3_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleDependencyStatement_in_rule__Component__DependsOnAssignment_3_3_14821);
            ruleDependencyStatement();

            state._fsp--;

             after(grammarAccess.getComponentAccess().getDependsOnDependencyStatementParserRuleCall_3_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__DependsOnAssignment_3_3_1"


    // $ANTLR start "rule__Options__NameAssignment_1"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2381:1: rule__Options__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Options__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2385:1: ( ( RULE_ID ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2386:1: ( RULE_ID )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2386:1: ( RULE_ID )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2387:1: RULE_ID
            {
             before(grammarAccess.getOptionsAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__Options__NameAssignment_14852); 
             after(grammarAccess.getOptionsAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Options__NameAssignment_1"


    // $ANTLR start "rule__Options__DisplayTypeAssignment_2"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2396:1: rule__Options__DisplayTypeAssignment_2 : ( ruleDisplayType ) ;
    public final void rule__Options__DisplayTypeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2400:1: ( ( ruleDisplayType ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2401:1: ( ruleDisplayType )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2401:1: ( ruleDisplayType )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2402:1: ruleDisplayType
            {
             before(grammarAccess.getOptionsAccess().getDisplayTypeDisplayTypeEnumRuleCall_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleDisplayType_in_rule__Options__DisplayTypeAssignment_24883);
            ruleDisplayType();

            state._fsp--;

             after(grammarAccess.getOptionsAccess().getDisplayTypeDisplayTypeEnumRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Options__DisplayTypeAssignment_2"


    // $ANTLR start "rule__Options__ComponentsAssignment_4"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2411:1: rule__Options__ComponentsAssignment_4 : ( ruleComponent ) ;
    public final void rule__Options__ComponentsAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2415:1: ( ( ruleComponent ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2416:1: ( ruleComponent )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2416:1: ( ruleComponent )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2417:1: ruleComponent
            {
             before(grammarAccess.getOptionsAccess().getComponentsComponentParserRuleCall_4_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleComponent_in_rule__Options__ComponentsAssignment_44914);
            ruleComponent();

            state._fsp--;

             after(grammarAccess.getOptionsAccess().getComponentsComponentParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Options__ComponentsAssignment_4"


    // $ANTLR start "rule__Options__ComponentsAssignment_5_1"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2426:1: rule__Options__ComponentsAssignment_5_1 : ( ruleComponent ) ;
    public final void rule__Options__ComponentsAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2430:1: ( ( ruleComponent ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2431:1: ( ruleComponent )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2431:1: ( ruleComponent )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2432:1: ruleComponent
            {
             before(grammarAccess.getOptionsAccess().getComponentsComponentParserRuleCall_5_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleComponent_in_rule__Options__ComponentsAssignment_5_14945);
            ruleComponent();

            state._fsp--;

             after(grammarAccess.getOptionsAccess().getComponentsComponentParserRuleCall_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Options__ComponentsAssignment_5_1"


    // $ANTLR start "rule__DependencyStatement__NameAssignment_0"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2441:1: rule__DependencyStatement__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__DependencyStatement__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2445:1: ( ( RULE_ID ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2446:1: ( RULE_ID )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2446:1: ( RULE_ID )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2447:1: RULE_ID
            {
             before(grammarAccess.getDependencyStatementAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__DependencyStatement__NameAssignment_04976); 
             after(grammarAccess.getDependencyStatementAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DependencyStatement__NameAssignment_0"


    // $ANTLR start "rule__DependencyStatement__DependenciesAssignment_2"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2456:1: rule__DependencyStatement__DependenciesAssignment_2 : ( ruleDependency ) ;
    public final void rule__DependencyStatement__DependenciesAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2460:1: ( ( ruleDependency ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2461:1: ( ruleDependency )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2461:1: ( ruleDependency )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2462:1: ruleDependency
            {
             before(grammarAccess.getDependencyStatementAccess().getDependenciesDependencyParserRuleCall_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleDependency_in_rule__DependencyStatement__DependenciesAssignment_25007);
            ruleDependency();

            state._fsp--;

             after(grammarAccess.getDependencyStatementAccess().getDependenciesDependencyParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DependencyStatement__DependenciesAssignment_2"


    // $ANTLR start "rule__UnDep__OpAssignment_0"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2471:1: rule__UnDep__OpAssignment_0 : ( ruleUnOperator ) ;
    public final void rule__UnDep__OpAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2475:1: ( ( ruleUnOperator ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2476:1: ( ruleUnOperator )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2476:1: ( ruleUnOperator )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2477:1: ruleUnOperator
            {
             before(grammarAccess.getUnDepAccess().getOpUnOperatorEnumRuleCall_0_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleUnOperator_in_rule__UnDep__OpAssignment_05038);
            ruleUnOperator();

            state._fsp--;

             after(grammarAccess.getUnDepAccess().getOpUnOperatorEnumRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnDep__OpAssignment_0"


    // $ANTLR start "rule__UnDep__DepAssignment_1"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2486:1: rule__UnDep__DepAssignment_1 : ( ruleTerm ) ;
    public final void rule__UnDep__DepAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2490:1: ( ( ruleTerm ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2491:1: ( ruleTerm )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2491:1: ( ruleTerm )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2492:1: ruleTerm
            {
             before(grammarAccess.getUnDepAccess().getDepTermParserRuleCall_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleTerm_in_rule__UnDep__DepAssignment_15069);
            ruleTerm();

            state._fsp--;

             after(grammarAccess.getUnDepAccess().getDepTermParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnDep__DepAssignment_1"


    // $ANTLR start "rule__Const__TypeAssignment_0_1_0"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2501:1: rule__Const__TypeAssignment_0_1_0 : ( ruleSimpleTypes ) ;
    public final void rule__Const__TypeAssignment_0_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2505:1: ( ( ruleSimpleTypes ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2506:1: ( ruleSimpleTypes )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2506:1: ( ruleSimpleTypes )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2507:1: ruleSimpleTypes
            {
             before(grammarAccess.getConstAccess().getTypeSimpleTypesEnumRuleCall_0_1_0_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleSimpleTypes_in_rule__Const__TypeAssignment_0_1_05100);
            ruleSimpleTypes();

            state._fsp--;

             after(grammarAccess.getConstAccess().getTypeSimpleTypesEnumRuleCall_0_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Const__TypeAssignment_0_1_0"


    // $ANTLR start "rule__Const__ValueAssignment_0_1_1"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2516:1: rule__Const__ValueAssignment_0_1_1 : ( ruleEString ) ;
    public final void rule__Const__ValueAssignment_0_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2520:1: ( ( ruleEString ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2521:1: ( ruleEString )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2521:1: ( ruleEString )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2522:1: ruleEString
            {
             before(grammarAccess.getConstAccess().getValueEStringParserRuleCall_0_1_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Const__ValueAssignment_0_1_15131);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getConstAccess().getValueEStringParserRuleCall_0_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Const__ValueAssignment_0_1_1"


    // $ANTLR start "rule__Const__ComponentAssignment_1"
    // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2531:1: rule__Const__ComponentAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__Const__ComponentAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2535:1: ( ( ( RULE_ID ) ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2536:1: ( ( RULE_ID ) )
            {
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2536:1: ( ( RULE_ID ) )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2537:1: ( RULE_ID )
            {
             before(grammarAccess.getConstAccess().getComponentComponentCrossReference_1_0()); 
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2538:1: ( RULE_ID )
            // ../dk.itu.mdd.conf.ui/src-gen/dk/itu/mdd/ui/contentassist/antlr/internal/InternalConf.g:2539:1: RULE_ID
            {
             before(grammarAccess.getConstAccess().getComponentComponentIDTerminalRuleCall_1_0_1()); 
            match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__Const__ComponentAssignment_15166); 
             after(grammarAccess.getConstAccess().getComponentComponentIDTerminalRuleCall_1_0_1()); 

            }

             after(grammarAccess.getConstAccess().getComponentComponentCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Const__ComponentAssignment_1"

    // Delegated rules


 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_ruleModel_in_entryRuleModel61 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleModel68 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Model__Group__0_in_ruleModel94 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleDependency_in_entryRuleDependency121 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleDependency128 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Dependency__Group__0_in_ruleDependency154 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleTerm_in_entryRuleTerm181 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleTerm188 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Term__Alternatives_in_ruleTerm214 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleComponent_in_entryRuleComponent241 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleComponent248 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Component__Group__0_in_ruleComponent274 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleOptions_in_entryRuleOptions301 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleOptions308 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Options__Group__0_in_ruleOptions334 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleDependencyStatement_in_entryRuleDependencyStatement361 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleDependencyStatement368 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__DependencyStatement__Group__0_in_ruleDependencyStatement394 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_entryRuleEString421 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleEString428 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EString__Alternatives_in_ruleEString454 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleUnDep_in_entryRuleUnDep483 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleUnDep490 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__UnDep__Group__0_in_ruleUnDep516 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleConst_in_entryRuleConst543 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleConst550 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Const__Alternatives_in_ruleConst576 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__SimpleTypes__Alternatives_in_ruleSimpleTypes613 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__DisplayType__Alternatives_in_ruleDisplayType649 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__BinOperator__Alternatives_in_ruleBinOperator685 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__UnOperator__Alternatives_in_ruleUnOperator721 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Term__Group_0__0_in_rule__Term__Alternatives756 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleUnDep_in_rule__Term__Alternatives774 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleConst_in_rule__Term__Alternatives791 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Component__ValueAssignment_2_0_in_rule__Component__Alternatives_2823 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Component__Group_2_1__0_in_rule__Component__Alternatives_2841 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_rule__EString__Alternatives874 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__EString__Alternatives891 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Const__Group_0__0_in_rule__Const__Alternatives923 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Const__ComponentAssignment_1_in_rule__Const__Alternatives941 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_11_in_rule__SimpleTypes__Alternatives975 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_12_in_rule__SimpleTypes__Alternatives996 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_13_in_rule__SimpleTypes__Alternatives1017 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_14_in_rule__SimpleTypes__Alternatives1038 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_15_in_rule__SimpleTypes__Alternatives1059 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_16_in_rule__SimpleTypes__Alternatives1080 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_17_in_rule__SimpleTypes__Alternatives1101 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_18_in_rule__DisplayType__Alternatives1137 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_19_in_rule__DisplayType__Alternatives1158 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_20_in_rule__DisplayType__Alternatives1179 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_21_in_rule__DisplayType__Alternatives1200 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_22_in_rule__BinOperator__Alternatives1236 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_23_in_rule__BinOperator__Alternatives1257 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_24_in_rule__BinOperator__Alternatives1278 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_25_in_rule__BinOperator__Alternatives1299 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_26_in_rule__BinOperator__Alternatives1320 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_27_in_rule__BinOperator__Alternatives1341 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_28_in_rule__BinOperator__Alternatives1362 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_29_in_rule__BinOperator__Alternatives1383 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_30_in_rule__UnOperator__Alternatives1419 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_31_in_rule__UnOperator__Alternatives1440 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Model__Group__0__Impl_in_rule__Model__Group__01473 = new BitSet(new long[]{0x000000000003F820L});
        public static final BitSet FOLLOW_rule__Model__Group__1_in_rule__Model__Group__01476 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Model__Group__1__Impl_in_rule__Model__Group__11534 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Model__Group_1__0_in_rule__Model__Group__1__Impl1561 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Model__Group_1__0__Impl_in_rule__Model__Group_1__01596 = new BitSet(new long[]{0x000000000003F820L});
        public static final BitSet FOLLOW_rule__Model__Group_1__1_in_rule__Model__Group_1__01599 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Model__RootAssignment_1_0_in_rule__Model__Group_1__0__Impl1626 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Model__Group_1__1__Impl_in_rule__Model__Group_1__11656 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Model__RootAssignment_1_1_in_rule__Model__Group_1__1__Impl1683 = new BitSet(new long[]{0x000000000003F822L});
        public static final BitSet FOLLOW_rule__Dependency__Group__0__Impl_in_rule__Dependency__Group__01718 = new BitSet(new long[]{0x000000003FC00000L});
        public static final BitSet FOLLOW_rule__Dependency__Group__1_in_rule__Dependency__Group__01721 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleTerm_in_rule__Dependency__Group__0__Impl1748 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Dependency__Group__1__Impl_in_rule__Dependency__Group__11777 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Dependency__Group_1__0_in_rule__Dependency__Group__1__Impl1804 = new BitSet(new long[]{0x000000003FC00002L});
        public static final BitSet FOLLOW_rule__Dependency__Group_1__0__Impl_in_rule__Dependency__Group_1__01839 = new BitSet(new long[]{0x000000003FC00000L});
        public static final BitSet FOLLOW_rule__Dependency__Group_1__1_in_rule__Dependency__Group_1__01842 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Dependency__Group_1__1__Impl_in_rule__Dependency__Group_1__11900 = new BitSet(new long[]{0x00000001C003F820L});
        public static final BitSet FOLLOW_rule__Dependency__Group_1__2_in_rule__Dependency__Group_1__11903 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Dependency__OpAssignment_1_1_in_rule__Dependency__Group_1__1__Impl1930 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Dependency__Group_1__2__Impl_in_rule__Dependency__Group_1__21960 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Dependency__RightDepAssignment_1_2_in_rule__Dependency__Group_1__2__Impl1987 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Term__Group_0__0__Impl_in_rule__Term__Group_0__02023 = new BitSet(new long[]{0x00000001C003F820L});
        public static final BitSet FOLLOW_rule__Term__Group_0__1_in_rule__Term__Group_0__02026 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_32_in_rule__Term__Group_0__0__Impl2054 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Term__Group_0__1__Impl_in_rule__Term__Group_0__12085 = new BitSet(new long[]{0x0000000200000000L});
        public static final BitSet FOLLOW_rule__Term__Group_0__2_in_rule__Term__Group_0__12088 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleDependency_in_rule__Term__Group_0__1__Impl2115 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Term__Group_0__2__Impl_in_rule__Term__Group_0__22144 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_33_in_rule__Term__Group_0__2__Impl2172 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Component__Group__0__Impl_in_rule__Component__Group__02209 = new BitSet(new long[]{0x000000000003F820L});
        public static final BitSet FOLLOW_rule__Component__Group__1_in_rule__Component__Group__02212 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Component__TypeAssignment_0_in_rule__Component__Group__0__Impl2239 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Component__Group__1__Impl_in_rule__Component__Group__12270 = new BitSet(new long[]{0x0000000400000030L});
        public static final BitSet FOLLOW_rule__Component__Group__2_in_rule__Component__Group__12273 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Component__NameAssignment_1_in_rule__Component__Group__1__Impl2300 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Component__Group__2__Impl_in_rule__Component__Group__22330 = new BitSet(new long[]{0x0000004000000000L});
        public static final BitSet FOLLOW_rule__Component__Group__3_in_rule__Component__Group__22333 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Component__Alternatives_2_in_rule__Component__Group__2__Impl2360 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Component__Group__3__Impl_in_rule__Component__Group__32390 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Component__Group_3__0_in_rule__Component__Group__3__Impl2417 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Component__Group_2_1__0__Impl_in_rule__Component__Group_2_1__02456 = new BitSet(new long[]{0x0000000800000000L});
        public static final BitSet FOLLOW_rule__Component__Group_2_1__1_in_rule__Component__Group_2_1__02459 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_34_in_rule__Component__Group_2_1__0__Impl2487 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Component__Group_2_1__1__Impl_in_rule__Component__Group_2_1__12518 = new BitSet(new long[]{0x0000008000000000L});
        public static final BitSet FOLLOW_rule__Component__Group_2_1__2_in_rule__Component__Group_2_1__12521 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_35_in_rule__Component__Group_2_1__1__Impl2549 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Component__Group_2_1__2__Impl_in_rule__Component__Group_2_1__22580 = new BitSet(new long[]{0x0000003000000000L});
        public static final BitSet FOLLOW_rule__Component__Group_2_1__3_in_rule__Component__Group_2_1__22583 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Component__AvailableOptionsAssignment_2_1_2_in_rule__Component__Group_2_1__2__Impl2610 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Component__Group_2_1__3__Impl_in_rule__Component__Group_2_1__32640 = new BitSet(new long[]{0x0000003000000000L});
        public static final BitSet FOLLOW_rule__Component__Group_2_1__4_in_rule__Component__Group_2_1__32643 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Component__Group_2_1_3__0_in_rule__Component__Group_2_1__3__Impl2670 = new BitSet(new long[]{0x0000002000000002L});
        public static final BitSet FOLLOW_rule__Component__Group_2_1__4__Impl_in_rule__Component__Group_2_1__42701 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_36_in_rule__Component__Group_2_1__4__Impl2729 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Component__Group_2_1_3__0__Impl_in_rule__Component__Group_2_1_3__02770 = new BitSet(new long[]{0x0000008000000000L});
        public static final BitSet FOLLOW_rule__Component__Group_2_1_3__1_in_rule__Component__Group_2_1_3__02773 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_37_in_rule__Component__Group_2_1_3__0__Impl2801 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Component__Group_2_1_3__1__Impl_in_rule__Component__Group_2_1_3__12832 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Component__AvailableOptionsAssignment_2_1_3_1_in_rule__Component__Group_2_1_3__1__Impl2859 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Component__Group_3__0__Impl_in_rule__Component__Group_3__02893 = new BitSet(new long[]{0x0000000800000000L});
        public static final BitSet FOLLOW_rule__Component__Group_3__1_in_rule__Component__Group_3__02896 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_38_in_rule__Component__Group_3__0__Impl2924 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Component__Group_3__1__Impl_in_rule__Component__Group_3__12955 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_rule__Component__Group_3__2_in_rule__Component__Group_3__12958 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_35_in_rule__Component__Group_3__1__Impl2986 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Component__Group_3__2__Impl_in_rule__Component__Group_3__23017 = new BitSet(new long[]{0x0000003000000000L});
        public static final BitSet FOLLOW_rule__Component__Group_3__3_in_rule__Component__Group_3__23020 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Component__DependsOnAssignment_3_2_in_rule__Component__Group_3__2__Impl3047 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Component__Group_3__3__Impl_in_rule__Component__Group_3__33077 = new BitSet(new long[]{0x0000003000000000L});
        public static final BitSet FOLLOW_rule__Component__Group_3__4_in_rule__Component__Group_3__33080 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Component__Group_3_3__0_in_rule__Component__Group_3__3__Impl3107 = new BitSet(new long[]{0x0000002000000002L});
        public static final BitSet FOLLOW_rule__Component__Group_3__4__Impl_in_rule__Component__Group_3__43138 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_36_in_rule__Component__Group_3__4__Impl3166 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Component__Group_3_3__0__Impl_in_rule__Component__Group_3_3__03207 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_rule__Component__Group_3_3__1_in_rule__Component__Group_3_3__03210 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_37_in_rule__Component__Group_3_3__0__Impl3238 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Component__Group_3_3__1__Impl_in_rule__Component__Group_3_3__13269 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Component__DependsOnAssignment_3_3_1_in_rule__Component__Group_3_3__1__Impl3296 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Options__Group__0__Impl_in_rule__Options__Group__03330 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_rule__Options__Group__1_in_rule__Options__Group__03333 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_39_in_rule__Options__Group__0__Impl3361 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Options__Group__1__Impl_in_rule__Options__Group__13392 = new BitSet(new long[]{0x00000008003C0000L});
        public static final BitSet FOLLOW_rule__Options__Group__2_in_rule__Options__Group__13395 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Options__NameAssignment_1_in_rule__Options__Group__1__Impl3422 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Options__Group__2__Impl_in_rule__Options__Group__23452 = new BitSet(new long[]{0x00000008003C0000L});
        public static final BitSet FOLLOW_rule__Options__Group__3_in_rule__Options__Group__23455 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Options__DisplayTypeAssignment_2_in_rule__Options__Group__2__Impl3482 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Options__Group__3__Impl_in_rule__Options__Group__33513 = new BitSet(new long[]{0x000000000003F820L});
        public static final BitSet FOLLOW_rule__Options__Group__4_in_rule__Options__Group__33516 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_35_in_rule__Options__Group__3__Impl3544 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Options__Group__4__Impl_in_rule__Options__Group__43575 = new BitSet(new long[]{0x0000003000000000L});
        public static final BitSet FOLLOW_rule__Options__Group__5_in_rule__Options__Group__43578 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Options__ComponentsAssignment_4_in_rule__Options__Group__4__Impl3605 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Options__Group__5__Impl_in_rule__Options__Group__53635 = new BitSet(new long[]{0x0000003000000000L});
        public static final BitSet FOLLOW_rule__Options__Group__6_in_rule__Options__Group__53638 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Options__Group_5__0_in_rule__Options__Group__5__Impl3665 = new BitSet(new long[]{0x0000002000000002L});
        public static final BitSet FOLLOW_rule__Options__Group__6__Impl_in_rule__Options__Group__63696 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_36_in_rule__Options__Group__6__Impl3724 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Options__Group_5__0__Impl_in_rule__Options__Group_5__03769 = new BitSet(new long[]{0x000000000003F820L});
        public static final BitSet FOLLOW_rule__Options__Group_5__1_in_rule__Options__Group_5__03772 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_37_in_rule__Options__Group_5__0__Impl3800 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Options__Group_5__1__Impl_in_rule__Options__Group_5__13831 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Options__ComponentsAssignment_5_1_in_rule__Options__Group_5__1__Impl3858 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__DependencyStatement__Group__0__Impl_in_rule__DependencyStatement__Group__03892 = new BitSet(new long[]{0x0000000100000000L});
        public static final BitSet FOLLOW_rule__DependencyStatement__Group__1_in_rule__DependencyStatement__Group__03895 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__DependencyStatement__NameAssignment_0_in_rule__DependencyStatement__Group__0__Impl3922 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__DependencyStatement__Group__1__Impl_in_rule__DependencyStatement__Group__13952 = new BitSet(new long[]{0x00000001C003F820L});
        public static final BitSet FOLLOW_rule__DependencyStatement__Group__2_in_rule__DependencyStatement__Group__13955 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_32_in_rule__DependencyStatement__Group__1__Impl3983 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__DependencyStatement__Group__2__Impl_in_rule__DependencyStatement__Group__24014 = new BitSet(new long[]{0x0000000200000000L});
        public static final BitSet FOLLOW_rule__DependencyStatement__Group__3_in_rule__DependencyStatement__Group__24017 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__DependencyStatement__DependenciesAssignment_2_in_rule__DependencyStatement__Group__2__Impl4044 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__DependencyStatement__Group__3__Impl_in_rule__DependencyStatement__Group__34074 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_33_in_rule__DependencyStatement__Group__3__Impl4102 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__UnDep__Group__0__Impl_in_rule__UnDep__Group__04142 = new BitSet(new long[]{0x00000001C003F820L});
        public static final BitSet FOLLOW_rule__UnDep__Group__1_in_rule__UnDep__Group__04145 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__UnDep__OpAssignment_0_in_rule__UnDep__Group__0__Impl4172 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__UnDep__Group__1__Impl_in_rule__UnDep__Group__14202 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__UnDep__DepAssignment_1_in_rule__UnDep__Group__1__Impl4229 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Const__Group_0__0__Impl_in_rule__Const__Group_0__04263 = new BitSet(new long[]{0x000000000003F800L});
        public static final BitSet FOLLOW_rule__Const__Group_0__1_in_rule__Const__Group_0__04266 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Const__Group_0__1__Impl_in_rule__Const__Group_0__14324 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Const__Group_0_1__0_in_rule__Const__Group_0__1__Impl4351 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Const__Group_0_1__0__Impl_in_rule__Const__Group_0_1__04385 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__Const__Group_0_1__1_in_rule__Const__Group_0_1__04388 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Const__TypeAssignment_0_1_0_in_rule__Const__Group_0_1__0__Impl4415 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Const__Group_0_1__1__Impl_in_rule__Const__Group_0_1__14445 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Const__ValueAssignment_0_1_1_in_rule__Const__Group_0_1__1__Impl4472 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleComponent_in_rule__Model__RootAssignment_1_04511 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleComponent_in_rule__Model__RootAssignment_1_14542 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBinOperator_in_rule__Dependency__OpAssignment_1_14573 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleTerm_in_rule__Dependency__RightDepAssignment_1_24604 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleSimpleTypes_in_rule__Component__TypeAssignment_04635 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__Component__NameAssignment_14666 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Component__ValueAssignment_2_04697 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleOptions_in_rule__Component__AvailableOptionsAssignment_2_1_24728 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleOptions_in_rule__Component__AvailableOptionsAssignment_2_1_3_14759 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleDependencyStatement_in_rule__Component__DependsOnAssignment_3_24790 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleDependencyStatement_in_rule__Component__DependsOnAssignment_3_3_14821 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__Options__NameAssignment_14852 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleDisplayType_in_rule__Options__DisplayTypeAssignment_24883 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleComponent_in_rule__Options__ComponentsAssignment_44914 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleComponent_in_rule__Options__ComponentsAssignment_5_14945 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__DependencyStatement__NameAssignment_04976 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleDependency_in_rule__DependencyStatement__DependenciesAssignment_25007 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleUnOperator_in_rule__UnDep__OpAssignment_05038 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleTerm_in_rule__UnDep__DepAssignment_15069 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleSimpleTypes_in_rule__Const__TypeAssignment_0_1_05100 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Const__ValueAssignment_0_1_15131 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__Const__ComponentAssignment_15166 = new BitSet(new long[]{0x0000000000000002L});
    }


}