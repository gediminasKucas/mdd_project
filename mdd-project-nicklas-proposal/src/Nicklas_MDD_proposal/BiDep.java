/**
 */
package Nicklas_MDD_proposal;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bi Dep</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link Nicklas_MDD_proposal.BiDep#getLeftDep <em>Left Dep</em>}</li>
 *   <li>{@link Nicklas_MDD_proposal.BiDep#getRightDep <em>Right Dep</em>}</li>
 *   <li>{@link Nicklas_MDD_proposal.BiDep#getOp <em>Op</em>}</li>
 * </ul>
 * </p>
 *
 * @see Nicklas_MDD_proposal.Nicklas_MDD_proposalPackage#getBiDep()
 * @model
 * @generated
 */
public interface BiDep extends Dependency {
	/**
	 * Returns the value of the '<em><b>Left Dep</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left Dep</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left Dep</em>' containment reference.
	 * @see #setLeftDep(Dependency)
	 * @see Nicklas_MDD_proposal.Nicklas_MDD_proposalPackage#getBiDep_LeftDep()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Dependency getLeftDep();

	/**
	 * Sets the value of the '{@link Nicklas_MDD_proposal.BiDep#getLeftDep <em>Left Dep</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left Dep</em>' containment reference.
	 * @see #getLeftDep()
	 * @generated
	 */
	void setLeftDep(Dependency value);

	/**
	 * Returns the value of the '<em><b>Right Dep</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right Dep</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right Dep</em>' containment reference.
	 * @see #setRightDep(Dependency)
	 * @see Nicklas_MDD_proposal.Nicklas_MDD_proposalPackage#getBiDep_RightDep()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Dependency getRightDep();

	/**
	 * Sets the value of the '{@link Nicklas_MDD_proposal.BiDep#getRightDep <em>Right Dep</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right Dep</em>' containment reference.
	 * @see #getRightDep()
	 * @generated
	 */
	void setRightDep(Dependency value);

	/**
	 * Returns the value of the '<em><b>Op</b></em>' attribute.
	 * The literals are from the enumeration {@link Nicklas_MDD_proposal.BinOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Op</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Op</em>' attribute.
	 * @see Nicklas_MDD_proposal.BinOperator
	 * @see #setOp(BinOperator)
	 * @see Nicklas_MDD_proposal.Nicklas_MDD_proposalPackage#getBiDep_Op()
	 * @model required="true"
	 * @generated
	 */
	BinOperator getOp();

	/**
	 * Sets the value of the '{@link Nicklas_MDD_proposal.BiDep#getOp <em>Op</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Op</em>' attribute.
	 * @see Nicklas_MDD_proposal.BinOperator
	 * @see #getOp()
	 * @generated
	 */
	void setOp(BinOperator value);

} // BiDep
