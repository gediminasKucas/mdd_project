/**
 */
package Nicklas_MDD_proposal;

import org.eclipse.emf.ecore.EObject;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Nicklas_MDD_proposal.Nicklas_MDD_proposalPackage#getDependency()
 * @model abstract="true"
 * @generated
 */
public interface Dependency extends EObject {

} // Dependency
