/**
 */
package Nicklas_MDD_proposal.impl;

import Nicklas_MDD_proposal.Dependency;
import Nicklas_MDD_proposal.Nicklas_MDD_proposalPackage;
import Nicklas_MDD_proposal.UnDep;
import Nicklas_MDD_proposal.UnOperator;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Un Dep</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link Nicklas_MDD_proposal.impl.UnDepImpl#getDep <em>Dep</em>}</li>
 *   <li>{@link Nicklas_MDD_proposal.impl.UnDepImpl#getOp <em>Op</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class UnDepImpl extends DependencyImpl implements UnDep {
	/**
	 * The cached value of the '{@link #getDep() <em>Dep</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDep()
	 * @generated
	 * @ordered
	 */
	protected Dependency dep;

	/**
	 * The default value of the '{@link #getOp() <em>Op</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOp()
	 * @generated
	 * @ordered
	 */
	protected static final UnOperator OP_EDEFAULT = UnOperator.TRUE;

	/**
	 * The cached value of the '{@link #getOp() <em>Op</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOp()
	 * @generated
	 * @ordered
	 */
	protected UnOperator op = OP_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UnDepImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Nicklas_MDD_proposalPackage.Literals.UN_DEP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dependency getDep() {
		return dep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDep(Dependency newDep, NotificationChain msgs) {
		Dependency oldDep = dep;
		dep = newDep;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Nicklas_MDD_proposalPackage.UN_DEP__DEP, oldDep, newDep);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDep(Dependency newDep) {
		if (newDep != dep) {
			NotificationChain msgs = null;
			if (dep != null)
				msgs = ((InternalEObject)dep).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - Nicklas_MDD_proposalPackage.UN_DEP__DEP, null, msgs);
			if (newDep != null)
				msgs = ((InternalEObject)newDep).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - Nicklas_MDD_proposalPackage.UN_DEP__DEP, null, msgs);
			msgs = basicSetDep(newDep, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Nicklas_MDD_proposalPackage.UN_DEP__DEP, newDep, newDep));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnOperator getOp() {
		return op;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOp(UnOperator newOp) {
		UnOperator oldOp = op;
		op = newOp == null ? OP_EDEFAULT : newOp;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Nicklas_MDD_proposalPackage.UN_DEP__OP, oldOp, op));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Nicklas_MDD_proposalPackage.UN_DEP__DEP:
				return basicSetDep(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Nicklas_MDD_proposalPackage.UN_DEP__DEP:
				return getDep();
			case Nicklas_MDD_proposalPackage.UN_DEP__OP:
				return getOp();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Nicklas_MDD_proposalPackage.UN_DEP__DEP:
				setDep((Dependency)newValue);
				return;
			case Nicklas_MDD_proposalPackage.UN_DEP__OP:
				setOp((UnOperator)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Nicklas_MDD_proposalPackage.UN_DEP__DEP:
				setDep((Dependency)null);
				return;
			case Nicklas_MDD_proposalPackage.UN_DEP__OP:
				setOp(OP_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Nicklas_MDD_proposalPackage.UN_DEP__DEP:
				return dep != null;
			case Nicklas_MDD_proposalPackage.UN_DEP__OP:
				return op != OP_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (op: ");
		result.append(op);
		result.append(')');
		return result.toString();
	}

} //UnDepImpl
