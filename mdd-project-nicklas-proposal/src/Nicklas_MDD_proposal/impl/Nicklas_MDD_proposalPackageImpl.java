/**
 */
package Nicklas_MDD_proposal.impl;

import Nicklas_MDD_proposal.BiDep;
import Nicklas_MDD_proposal.BinOperator;
import Nicklas_MDD_proposal.Component;
import Nicklas_MDD_proposal.Const;
import Nicklas_MDD_proposal.Dependency;
import Nicklas_MDD_proposal.DependencyStatement;
import Nicklas_MDD_proposal.DisplayType;
import Nicklas_MDD_proposal.Model;
import Nicklas_MDD_proposal.NamedElement;
import Nicklas_MDD_proposal.Nicklas_MDD_proposalFactory;
import Nicklas_MDD_proposal.Nicklas_MDD_proposalPackage;
import Nicklas_MDD_proposal.Options;
import Nicklas_MDD_proposal.SimpleTypes;
import Nicklas_MDD_proposal.UnDep;
import Nicklas_MDD_proposal.UnOperator;
import Nicklas_MDD_proposal.ValuedElement;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Nicklas_MDD_proposalPackageImpl extends EPackageImpl implements Nicklas_MDD_proposalPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass biDepEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unDepEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass valuedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dependencyStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass optionsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum simpleTypesEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum displayTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum binOperatorEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum unOperatorEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see Nicklas_MDD_proposal.Nicklas_MDD_proposalPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Nicklas_MDD_proposalPackageImpl() {
		super(eNS_URI, Nicklas_MDD_proposalFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link Nicklas_MDD_proposalPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static Nicklas_MDD_proposalPackage init() {
		if (isInited) return (Nicklas_MDD_proposalPackage)EPackage.Registry.INSTANCE.getEPackage(Nicklas_MDD_proposalPackage.eNS_URI);

		// Obtain or create and register package
		Nicklas_MDD_proposalPackageImpl theNicklas_MDD_proposalPackage = (Nicklas_MDD_proposalPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof Nicklas_MDD_proposalPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new Nicklas_MDD_proposalPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theNicklas_MDD_proposalPackage.createPackageContents();

		// Initialize created meta-data
		theNicklas_MDD_proposalPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theNicklas_MDD_proposalPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(Nicklas_MDD_proposalPackage.eNS_URI, theNicklas_MDD_proposalPackage);
		return theNicklas_MDD_proposalPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModel() {
		return modelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModel_Root() {
		return (EReference)modelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComponent() {
		return componentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponent_AvailableOptions() {
		return (EReference)componentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponent_DependsOn() {
		return (EReference)componentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDependency() {
		return dependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBiDep() {
		return biDepEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBiDep_LeftDep() {
		return (EReference)biDepEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBiDep_RightDep() {
		return (EReference)biDepEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBiDep_Op() {
		return (EAttribute)biDepEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUnDep() {
		return unDepEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUnDep_Dep() {
		return (EReference)unDepEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUnDep_Op() {
		return (EAttribute)unDepEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamedElement() {
		return namedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedElement_Name() {
		return (EAttribute)namedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConst() {
		return constEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConst_Component() {
		return (EReference)constEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getValuedElement() {
		return valuedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getValuedElement_Type() {
		return (EAttribute)valuedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getValuedElement_Value() {
		return (EAttribute)valuedElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDependencyStatement() {
		return dependencyStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDependencyStatement_Dependencies() {
		return (EReference)dependencyStatementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOptions() {
		return optionsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOptions_Components() {
		return (EReference)optionsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOptions_DisplayType() {
		return (EAttribute)optionsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSimpleTypes() {
		return simpleTypesEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDisplayType() {
		return displayTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getBinOperator() {
		return binOperatorEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getUnOperator() {
		return unOperatorEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Nicklas_MDD_proposalFactory getNicklas_MDD_proposalFactory() {
		return (Nicklas_MDD_proposalFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		modelEClass = createEClass(MODEL);
		createEReference(modelEClass, MODEL__ROOT);

		componentEClass = createEClass(COMPONENT);
		createEReference(componentEClass, COMPONENT__DEPENDS_ON);
		createEReference(componentEClass, COMPONENT__AVAILABLE_OPTIONS);

		dependencyEClass = createEClass(DEPENDENCY);

		biDepEClass = createEClass(BI_DEP);
		createEReference(biDepEClass, BI_DEP__LEFT_DEP);
		createEReference(biDepEClass, BI_DEP__RIGHT_DEP);
		createEAttribute(biDepEClass, BI_DEP__OP);

		unDepEClass = createEClass(UN_DEP);
		createEReference(unDepEClass, UN_DEP__DEP);
		createEAttribute(unDepEClass, UN_DEP__OP);

		namedElementEClass = createEClass(NAMED_ELEMENT);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__NAME);

		constEClass = createEClass(CONST);
		createEReference(constEClass, CONST__COMPONENT);

		valuedElementEClass = createEClass(VALUED_ELEMENT);
		createEAttribute(valuedElementEClass, VALUED_ELEMENT__TYPE);
		createEAttribute(valuedElementEClass, VALUED_ELEMENT__VALUE);

		dependencyStatementEClass = createEClass(DEPENDENCY_STATEMENT);
		createEReference(dependencyStatementEClass, DEPENDENCY_STATEMENT__DEPENDENCIES);

		optionsEClass = createEClass(OPTIONS);
		createEReference(optionsEClass, OPTIONS__COMPONENTS);
		createEAttribute(optionsEClass, OPTIONS__DISPLAY_TYPE);

		// Create enums
		simpleTypesEEnum = createEEnum(SIMPLE_TYPES);
		displayTypeEEnum = createEEnum(DISPLAY_TYPE);
		binOperatorEEnum = createEEnum(BIN_OPERATOR);
		unOperatorEEnum = createEEnum(UN_OPERATOR);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		componentEClass.getESuperTypes().add(this.getNamedElement());
		componentEClass.getESuperTypes().add(this.getValuedElement());
		biDepEClass.getESuperTypes().add(this.getDependency());
		unDepEClass.getESuperTypes().add(this.getDependency());
		constEClass.getESuperTypes().add(this.getDependency());
		constEClass.getESuperTypes().add(this.getValuedElement());
		dependencyStatementEClass.getESuperTypes().add(this.getNamedElement());
		optionsEClass.getESuperTypes().add(this.getNamedElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(modelEClass, Model.class, "Model", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getModel_Root(), this.getComponent(), null, "root", null, 0, -1, Model.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(componentEClass, Component.class, "Component", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getComponent_DependsOn(), this.getDependencyStatement(), null, "dependsOn", null, 0, -1, Component.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComponent_AvailableOptions(), this.getOptions(), null, "availableOptions", null, 0, -1, Component.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dependencyEClass, Dependency.class, "Dependency", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(biDepEClass, BiDep.class, "BiDep", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBiDep_LeftDep(), this.getDependency(), null, "leftDep", null, 1, 1, BiDep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBiDep_RightDep(), this.getDependency(), null, "rightDep", null, 1, 1, BiDep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBiDep_Op(), this.getBinOperator(), "op", null, 1, 1, BiDep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(unDepEClass, UnDep.class, "UnDep", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUnDep_Dep(), this.getDependency(), null, "Dep", null, 1, 1, UnDep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUnDep_Op(), this.getUnOperator(), "op", null, 0, 1, UnDep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(namedElementEClass, NamedElement.class, "NamedElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNamedElement_Name(), ecorePackage.getEString(), "name", null, 1, 1, NamedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(constEClass, Const.class, "Const", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConst_Component(), this.getComponent(), null, "component", null, 0, 1, Const.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(valuedElementEClass, ValuedElement.class, "ValuedElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getValuedElement_Type(), this.getSimpleTypes(), "Type", null, 0, 1, ValuedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getValuedElement_Value(), ecorePackage.getEString(), "Value", null, 0, 1, ValuedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dependencyStatementEClass, DependencyStatement.class, "DependencyStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDependencyStatement_Dependencies(), this.getDependency(), null, "dependencies", null, 1, 1, DependencyStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(optionsEClass, Options.class, "Options", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOptions_Components(), this.getComponent(), null, "components", null, 1, -1, Options.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOptions_DisplayType(), this.getDisplayType(), "DisplayType", null, 1, 1, Options.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(simpleTypesEEnum, SimpleTypes.class, "SimpleTypes");
		addEEnumLiteral(simpleTypesEEnum, SimpleTypes.BOOLEAN);
		addEEnumLiteral(simpleTypesEEnum, SimpleTypes.STRING);
		addEEnumLiteral(simpleTypesEEnum, SimpleTypes.INT);
		addEEnumLiteral(simpleTypesEEnum, SimpleTypes.CHAR);
		addEEnumLiteral(simpleTypesEEnum, SimpleTypes.FLOAT);
		addEEnumLiteral(simpleTypesEEnum, SimpleTypes.DOUBLE);
		addEEnumLiteral(simpleTypesEEnum, SimpleTypes.OBJECT);

		initEEnum(displayTypeEEnum, DisplayType.class, "DisplayType");
		addEEnumLiteral(displayTypeEEnum, DisplayType.RANGE);
		addEEnumLiteral(displayTypeEEnum, DisplayType.SELECT);
		addEEnumLiteral(displayTypeEEnum, DisplayType.MULTI_SELECT);
		addEEnumLiteral(displayTypeEEnum, DisplayType.CHECK_BOX);

		initEEnum(binOperatorEEnum, BinOperator.class, "BinOperator");
		addEEnumLiteral(binOperatorEEnum, BinOperator.EQUAL);
		addEEnumLiteral(binOperatorEEnum, BinOperator.NOT_EQUAL);
		addEEnumLiteral(binOperatorEEnum, BinOperator.LESS);
		addEEnumLiteral(binOperatorEEnum, BinOperator.GREATER);
		addEEnumLiteral(binOperatorEEnum, BinOperator.GREATER_EQUAL);
		addEEnumLiteral(binOperatorEEnum, BinOperator.LESS_EQUAL);
		addEEnumLiteral(binOperatorEEnum, BinOperator.AND);
		addEEnumLiteral(binOperatorEEnum, BinOperator.OR);

		initEEnum(unOperatorEEnum, UnOperator.class, "UnOperator");
		addEEnumLiteral(unOperatorEEnum, UnOperator.TRUE);
		addEEnumLiteral(unOperatorEEnum, UnOperator.FALSE);

		// Create resource
		createResource(eNS_URI);
	}

} //Nicklas_MDD_proposalPackageImpl
