/**
 */
package Nicklas_MDD_proposal.impl;

import Nicklas_MDD_proposal.Component;
import Nicklas_MDD_proposal.DependencyStatement;
import Nicklas_MDD_proposal.Nicklas_MDD_proposalPackage;
import Nicklas_MDD_proposal.Options;
import Nicklas_MDD_proposal.SimpleTypes;
import Nicklas_MDD_proposal.ValuedElement;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link Nicklas_MDD_proposal.impl.ComponentImpl#getType <em>Type</em>}</li>
 *   <li>{@link Nicklas_MDD_proposal.impl.ComponentImpl#getValue <em>Value</em>}</li>
 *   <li>{@link Nicklas_MDD_proposal.impl.ComponentImpl#getDependsOn <em>Depends On</em>}</li>
 *   <li>{@link Nicklas_MDD_proposal.impl.ComponentImpl#getAvailableOptions <em>Available Options</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ComponentImpl extends NamedElementImpl implements Component {
	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final SimpleTypes TYPE_EDEFAULT = SimpleTypes.BOOLEAN;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected SimpleTypes type = TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected String value = VALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDependsOn() <em>Depends On</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDependsOn()
	 * @generated
	 * @ordered
	 */
	protected EList<DependencyStatement> dependsOn;

	/**
	 * The cached value of the '{@link #getAvailableOptions() <em>Available Options</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvailableOptions()
	 * @generated
	 * @ordered
	 */
	protected EList<Options> availableOptions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Nicklas_MDD_proposalPackage.Literals.COMPONENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleTypes getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(SimpleTypes newType) {
		SimpleTypes oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Nicklas_MDD_proposalPackage.COMPONENT__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(String newValue) {
		String oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Nicklas_MDD_proposalPackage.COMPONENT__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Options> getAvailableOptions() {
		if (availableOptions == null) {
			availableOptions = new EObjectContainmentEList<Options>(Options.class, this, Nicklas_MDD_proposalPackage.COMPONENT__AVAILABLE_OPTIONS);
		}
		return availableOptions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DependencyStatement> getDependsOn() {
		if (dependsOn == null) {
			dependsOn = new EObjectContainmentEList<DependencyStatement>(DependencyStatement.class, this, Nicklas_MDD_proposalPackage.COMPONENT__DEPENDS_ON);
		}
		return dependsOn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Nicklas_MDD_proposalPackage.COMPONENT__DEPENDS_ON:
				return ((InternalEList<?>)getDependsOn()).basicRemove(otherEnd, msgs);
			case Nicklas_MDD_proposalPackage.COMPONENT__AVAILABLE_OPTIONS:
				return ((InternalEList<?>)getAvailableOptions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Nicklas_MDD_proposalPackage.COMPONENT__TYPE:
				return getType();
			case Nicklas_MDD_proposalPackage.COMPONENT__VALUE:
				return getValue();
			case Nicklas_MDD_proposalPackage.COMPONENT__DEPENDS_ON:
				return getDependsOn();
			case Nicklas_MDD_proposalPackage.COMPONENT__AVAILABLE_OPTIONS:
				return getAvailableOptions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Nicklas_MDD_proposalPackage.COMPONENT__TYPE:
				setType((SimpleTypes)newValue);
				return;
			case Nicklas_MDD_proposalPackage.COMPONENT__VALUE:
				setValue((String)newValue);
				return;
			case Nicklas_MDD_proposalPackage.COMPONENT__DEPENDS_ON:
				getDependsOn().clear();
				getDependsOn().addAll((Collection<? extends DependencyStatement>)newValue);
				return;
			case Nicklas_MDD_proposalPackage.COMPONENT__AVAILABLE_OPTIONS:
				getAvailableOptions().clear();
				getAvailableOptions().addAll((Collection<? extends Options>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Nicklas_MDD_proposalPackage.COMPONENT__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case Nicklas_MDD_proposalPackage.COMPONENT__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case Nicklas_MDD_proposalPackage.COMPONENT__DEPENDS_ON:
				getDependsOn().clear();
				return;
			case Nicklas_MDD_proposalPackage.COMPONENT__AVAILABLE_OPTIONS:
				getAvailableOptions().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Nicklas_MDD_proposalPackage.COMPONENT__TYPE:
				return type != TYPE_EDEFAULT;
			case Nicklas_MDD_proposalPackage.COMPONENT__VALUE:
				return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
			case Nicklas_MDD_proposalPackage.COMPONENT__DEPENDS_ON:
				return dependsOn != null && !dependsOn.isEmpty();
			case Nicklas_MDD_proposalPackage.COMPONENT__AVAILABLE_OPTIONS:
				return availableOptions != null && !availableOptions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == ValuedElement.class) {
			switch (derivedFeatureID) {
				case Nicklas_MDD_proposalPackage.COMPONENT__TYPE: return Nicklas_MDD_proposalPackage.VALUED_ELEMENT__TYPE;
				case Nicklas_MDD_proposalPackage.COMPONENT__VALUE: return Nicklas_MDD_proposalPackage.VALUED_ELEMENT__VALUE;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == ValuedElement.class) {
			switch (baseFeatureID) {
				case Nicklas_MDD_proposalPackage.VALUED_ELEMENT__TYPE: return Nicklas_MDD_proposalPackage.COMPONENT__TYPE;
				case Nicklas_MDD_proposalPackage.VALUED_ELEMENT__VALUE: return Nicklas_MDD_proposalPackage.COMPONENT__VALUE;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Type: ");
		result.append(type);
		result.append(", Value: ");
		result.append(value);
		result.append(')');
		return result.toString();
	}

} //ComponentImpl
