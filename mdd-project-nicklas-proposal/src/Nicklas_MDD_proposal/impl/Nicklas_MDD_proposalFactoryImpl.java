/**
 */
package Nicklas_MDD_proposal.impl;

import Nicklas_MDD_proposal.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Nicklas_MDD_proposalFactoryImpl extends EFactoryImpl implements Nicklas_MDD_proposalFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Nicklas_MDD_proposalFactory init() {
		try {
			Nicklas_MDD_proposalFactory theNicklas_MDD_proposalFactory = (Nicklas_MDD_proposalFactory)EPackage.Registry.INSTANCE.getEFactory(Nicklas_MDD_proposalPackage.eNS_URI);
			if (theNicklas_MDD_proposalFactory != null) {
				return theNicklas_MDD_proposalFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Nicklas_MDD_proposalFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Nicklas_MDD_proposalFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case Nicklas_MDD_proposalPackage.MODEL: return createModel();
			case Nicklas_MDD_proposalPackage.COMPONENT: return createComponent();
			case Nicklas_MDD_proposalPackage.BI_DEP: return createBiDep();
			case Nicklas_MDD_proposalPackage.UN_DEP: return createUnDep();
			case Nicklas_MDD_proposalPackage.CONST: return createConst();
			case Nicklas_MDD_proposalPackage.DEPENDENCY_STATEMENT: return createDependencyStatement();
			case Nicklas_MDD_proposalPackage.OPTIONS: return createOptions();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case Nicklas_MDD_proposalPackage.SIMPLE_TYPES:
				return createSimpleTypesFromString(eDataType, initialValue);
			case Nicklas_MDD_proposalPackage.DISPLAY_TYPE:
				return createDisplayTypeFromString(eDataType, initialValue);
			case Nicklas_MDD_proposalPackage.BIN_OPERATOR:
				return createBinOperatorFromString(eDataType, initialValue);
			case Nicklas_MDD_proposalPackage.UN_OPERATOR:
				return createUnOperatorFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case Nicklas_MDD_proposalPackage.SIMPLE_TYPES:
				return convertSimpleTypesToString(eDataType, instanceValue);
			case Nicklas_MDD_proposalPackage.DISPLAY_TYPE:
				return convertDisplayTypeToString(eDataType, instanceValue);
			case Nicklas_MDD_proposalPackage.BIN_OPERATOR:
				return convertBinOperatorToString(eDataType, instanceValue);
			case Nicklas_MDD_proposalPackage.UN_OPERATOR:
				return convertUnOperatorToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Model createModel() {
		ModelImpl model = new ModelImpl();
		return model;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Component createComponent() {
		ComponentImpl component = new ComponentImpl();
		return component;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BiDep createBiDep() {
		BiDepImpl biDep = new BiDepImpl();
		return biDep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnDep createUnDep() {
		UnDepImpl unDep = new UnDepImpl();
		return unDep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Const createConst() {
		ConstImpl const_ = new ConstImpl();
		return const_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DependencyStatement createDependencyStatement() {
		DependencyStatementImpl dependencyStatement = new DependencyStatementImpl();
		return dependencyStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Options createOptions() {
		OptionsImpl options = new OptionsImpl();
		return options;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleTypes createSimpleTypesFromString(EDataType eDataType, String initialValue) {
		SimpleTypes result = SimpleTypes.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSimpleTypesToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DisplayType createDisplayTypeFromString(EDataType eDataType, String initialValue) {
		DisplayType result = DisplayType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDisplayTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BinOperator createBinOperatorFromString(EDataType eDataType, String initialValue) {
		BinOperator result = BinOperator.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertBinOperatorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnOperator createUnOperatorFromString(EDataType eDataType, String initialValue) {
		UnOperator result = UnOperator.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertUnOperatorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Nicklas_MDD_proposalPackage getNicklas_MDD_proposalPackage() {
		return (Nicklas_MDD_proposalPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Nicklas_MDD_proposalPackage getPackage() {
		return Nicklas_MDD_proposalPackage.eINSTANCE;
	}

} //Nicklas_MDD_proposalFactoryImpl
