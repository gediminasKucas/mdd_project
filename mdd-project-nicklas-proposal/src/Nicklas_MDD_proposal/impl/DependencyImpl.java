/**
 */
package Nicklas_MDD_proposal.impl;

import Nicklas_MDD_proposal.Dependency;
import Nicklas_MDD_proposal.Nicklas_MDD_proposalPackage;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dependency</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public abstract class DependencyImpl extends MinimalEObjectImpl.Container implements Dependency {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DependencyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Nicklas_MDD_proposalPackage.Literals.DEPENDENCY;
	}

} //DependencyImpl
