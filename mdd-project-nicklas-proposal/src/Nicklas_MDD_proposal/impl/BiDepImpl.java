/**
 */
package Nicklas_MDD_proposal.impl;

import Nicklas_MDD_proposal.BiDep;
import Nicklas_MDD_proposal.BinOperator;
import Nicklas_MDD_proposal.Dependency;
import Nicklas_MDD_proposal.Nicklas_MDD_proposalPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Bi Dep</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link Nicklas_MDD_proposal.impl.BiDepImpl#getLeftDep <em>Left Dep</em>}</li>
 *   <li>{@link Nicklas_MDD_proposal.impl.BiDepImpl#getRightDep <em>Right Dep</em>}</li>
 *   <li>{@link Nicklas_MDD_proposal.impl.BiDepImpl#getOp <em>Op</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BiDepImpl extends DependencyImpl implements BiDep {
	/**
	 * The cached value of the '{@link #getLeftDep() <em>Left Dep</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeftDep()
	 * @generated
	 * @ordered
	 */
	protected Dependency leftDep;

	/**
	 * The cached value of the '{@link #getRightDep() <em>Right Dep</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRightDep()
	 * @generated
	 * @ordered
	 */
	protected Dependency rightDep;

	/**
	 * The default value of the '{@link #getOp() <em>Op</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOp()
	 * @generated
	 * @ordered
	 */
	protected static final BinOperator OP_EDEFAULT = BinOperator.EQUAL;

	/**
	 * The cached value of the '{@link #getOp() <em>Op</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOp()
	 * @generated
	 * @ordered
	 */
	protected BinOperator op = OP_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BiDepImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Nicklas_MDD_proposalPackage.Literals.BI_DEP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dependency getLeftDep() {
		return leftDep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLeftDep(Dependency newLeftDep, NotificationChain msgs) {
		Dependency oldLeftDep = leftDep;
		leftDep = newLeftDep;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Nicklas_MDD_proposalPackage.BI_DEP__LEFT_DEP, oldLeftDep, newLeftDep);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLeftDep(Dependency newLeftDep) {
		if (newLeftDep != leftDep) {
			NotificationChain msgs = null;
			if (leftDep != null)
				msgs = ((InternalEObject)leftDep).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - Nicklas_MDD_proposalPackage.BI_DEP__LEFT_DEP, null, msgs);
			if (newLeftDep != null)
				msgs = ((InternalEObject)newLeftDep).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - Nicklas_MDD_proposalPackage.BI_DEP__LEFT_DEP, null, msgs);
			msgs = basicSetLeftDep(newLeftDep, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Nicklas_MDD_proposalPackage.BI_DEP__LEFT_DEP, newLeftDep, newLeftDep));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dependency getRightDep() {
		return rightDep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRightDep(Dependency newRightDep, NotificationChain msgs) {
		Dependency oldRightDep = rightDep;
		rightDep = newRightDep;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Nicklas_MDD_proposalPackage.BI_DEP__RIGHT_DEP, oldRightDep, newRightDep);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRightDep(Dependency newRightDep) {
		if (newRightDep != rightDep) {
			NotificationChain msgs = null;
			if (rightDep != null)
				msgs = ((InternalEObject)rightDep).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - Nicklas_MDD_proposalPackage.BI_DEP__RIGHT_DEP, null, msgs);
			if (newRightDep != null)
				msgs = ((InternalEObject)newRightDep).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - Nicklas_MDD_proposalPackage.BI_DEP__RIGHT_DEP, null, msgs);
			msgs = basicSetRightDep(newRightDep, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Nicklas_MDD_proposalPackage.BI_DEP__RIGHT_DEP, newRightDep, newRightDep));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BinOperator getOp() {
		return op;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOp(BinOperator newOp) {
		BinOperator oldOp = op;
		op = newOp == null ? OP_EDEFAULT : newOp;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Nicklas_MDD_proposalPackage.BI_DEP__OP, oldOp, op));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Nicklas_MDD_proposalPackage.BI_DEP__LEFT_DEP:
				return basicSetLeftDep(null, msgs);
			case Nicklas_MDD_proposalPackage.BI_DEP__RIGHT_DEP:
				return basicSetRightDep(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Nicklas_MDD_proposalPackage.BI_DEP__LEFT_DEP:
				return getLeftDep();
			case Nicklas_MDD_proposalPackage.BI_DEP__RIGHT_DEP:
				return getRightDep();
			case Nicklas_MDD_proposalPackage.BI_DEP__OP:
				return getOp();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Nicklas_MDD_proposalPackage.BI_DEP__LEFT_DEP:
				setLeftDep((Dependency)newValue);
				return;
			case Nicklas_MDD_proposalPackage.BI_DEP__RIGHT_DEP:
				setRightDep((Dependency)newValue);
				return;
			case Nicklas_MDD_proposalPackage.BI_DEP__OP:
				setOp((BinOperator)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Nicklas_MDD_proposalPackage.BI_DEP__LEFT_DEP:
				setLeftDep((Dependency)null);
				return;
			case Nicklas_MDD_proposalPackage.BI_DEP__RIGHT_DEP:
				setRightDep((Dependency)null);
				return;
			case Nicklas_MDD_proposalPackage.BI_DEP__OP:
				setOp(OP_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Nicklas_MDD_proposalPackage.BI_DEP__LEFT_DEP:
				return leftDep != null;
			case Nicklas_MDD_proposalPackage.BI_DEP__RIGHT_DEP:
				return rightDep != null;
			case Nicklas_MDD_proposalPackage.BI_DEP__OP:
				return op != OP_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (op: ");
		result.append(op);
		result.append(')');
		return result.toString();
	}

} //BiDepImpl
