/**
 */
package Nicklas_MDD_proposal;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dependency Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link Nicklas_MDD_proposal.DependencyStatement#getDependencies <em>Dependencies</em>}</li>
 * </ul>
 * </p>
 *
 * @see Nicklas_MDD_proposal.Nicklas_MDD_proposalPackage#getDependencyStatement()
 * @model
 * @generated
 */
public interface DependencyStatement extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Dependencies</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dependencies</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dependencies</em>' containment reference.
	 * @see #setDependencies(Dependency)
	 * @see Nicklas_MDD_proposal.Nicklas_MDD_proposalPackage#getDependencyStatement_Dependencies()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Dependency getDependencies();

	/**
	 * Sets the value of the '{@link Nicklas_MDD_proposal.DependencyStatement#getDependencies <em>Dependencies</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dependencies</em>' containment reference.
	 * @see #getDependencies()
	 * @generated
	 */
	void setDependencies(Dependency value);

} // DependencyStatement
