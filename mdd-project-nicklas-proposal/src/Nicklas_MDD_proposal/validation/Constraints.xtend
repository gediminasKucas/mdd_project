package Nicklas_MDD_proposal.validation

import Nicklas_MDD_proposal.Component
import Nicklas_MDD_proposal.DisplayType
import Nicklas_MDD_proposal.Options
import Nicklas_MDD_proposal.SimpleTypes
import Nicklas_MDD_proposal.ValuedElement
import javax.management.openmbean.SimpleType

class Constraints {

	def static dispatch boolean constraint(Component it) {
		
		//Component cant have simpleType and options
		(type != null && availableOptions == null)
		&&
		//If type is present a value should be set
		type != null && value != null
		&&
		//If type isnt present available option should be set
		(type == null && (availableOptions != null && !availableOptions.isEmpty))
		
	}
	
	def static dispatch boolean constraint(Options it){
		
		
		
		//all options should have the same type
		components.forall[x | x.type == components.get(0).type]
		&&
		//if display type is range you should specify integer range values
		(displayType == DisplayType.RANGE && components.size <= 3 && components.size >= 2 
		&& components.forall[x | x.type ==SimpleType.INTEGER])
		
	}

	def static dispatch boolean constraint(ValuedElement it){
		(type == SimpleTypes.BOOLEAN && (value.equalsIgnoreCase('true') || value.equalsIgnoreCase('false')))
		&&
		type == SimpleTypes.INT && isInt(value)
		&&
		type == SimpleTypes.DOUBLE && isDouble(value)
		&&
		type == SimpleTypes.FLOAT && isFloat(value)
		&&
		type == SimpleTypes.CHAR && value.length == 1
	}
	
	def static boolean isInt(String value){
		try{
			Integer.parseInt(value);
		}catch(NumberFormatException e){
			return false
		}
		return true;
	}

	def static boolean isDouble(String value){
		try{
			Double.parseDouble(value);
		}catch(NumberFormatException e){
			return false
		}
		return true;
	}

	def static boolean isFloat(String value){
		try{
			Float.parseFloat(value);
		}catch(NumberFormatException e){
			return false
		}
		return true;
	}
	
}
