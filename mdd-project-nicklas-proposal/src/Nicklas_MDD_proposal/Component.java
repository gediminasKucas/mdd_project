/**
 */
package Nicklas_MDD_proposal;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link Nicklas_MDD_proposal.Component#getDependsOn <em>Depends On</em>}</li>
 *   <li>{@link Nicklas_MDD_proposal.Component#getAvailableOptions <em>Available Options</em>}</li>
 * </ul>
 * </p>
 *
 * @see Nicklas_MDD_proposal.Nicklas_MDD_proposalPackage#getComponent()
 * @model
 * @generated
 */
public interface Component extends NamedElement, ValuedElement {
	/**
	 * Returns the value of the '<em><b>Available Options</b></em>' containment reference list.
	 * The list contents are of type {@link Nicklas_MDD_proposal.Options}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Available Options</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Available Options</em>' containment reference list.
	 * @see Nicklas_MDD_proposal.Nicklas_MDD_proposalPackage#getComponent_AvailableOptions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Options> getAvailableOptions();

	/**
	 * Returns the value of the '<em><b>Depends On</b></em>' containment reference list.
	 * The list contents are of type {@link Nicklas_MDD_proposal.DependencyStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Depends On</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Depends On</em>' containment reference list.
	 * @see Nicklas_MDD_proposal.Nicklas_MDD_proposalPackage#getComponent_DependsOn()
	 * @model containment="true"
	 * @generated
	 */
	EList<DependencyStatement> getDependsOn();

} // Component
