/**
 */
package Nicklas_MDD_proposal;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Options</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link Nicklas_MDD_proposal.Options#getComponents <em>Components</em>}</li>
 *   <li>{@link Nicklas_MDD_proposal.Options#getDisplayType <em>Display Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see Nicklas_MDD_proposal.Nicklas_MDD_proposalPackage#getOptions()
 * @model
 * @generated
 */
public interface Options extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Components</b></em>' containment reference list.
	 * The list contents are of type {@link Nicklas_MDD_proposal.Component}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Components</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Components</em>' containment reference list.
	 * @see Nicklas_MDD_proposal.Nicklas_MDD_proposalPackage#getOptions_Components()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Component> getComponents();

	/**
	 * Returns the value of the '<em><b>Display Type</b></em>' attribute.
	 * The literals are from the enumeration {@link Nicklas_MDD_proposal.DisplayType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Display Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Display Type</em>' attribute.
	 * @see Nicklas_MDD_proposal.DisplayType
	 * @see #setDisplayType(DisplayType)
	 * @see Nicklas_MDD_proposal.Nicklas_MDD_proposalPackage#getOptions_DisplayType()
	 * @model required="true"
	 * @generated
	 */
	DisplayType getDisplayType();

	/**
	 * Sets the value of the '{@link Nicklas_MDD_proposal.Options#getDisplayType <em>Display Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Display Type</em>' attribute.
	 * @see Nicklas_MDD_proposal.DisplayType
	 * @see #getDisplayType()
	 * @generated
	 */
	void setDisplayType(DisplayType value);

} // Options
