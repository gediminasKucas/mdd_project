/**
 */
package Nicklas_MDD_proposal;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see Nicklas_MDD_proposal.Nicklas_MDD_proposalFactory
 * @model kind="package"
 * @generated
 */
public interface Nicklas_MDD_proposalPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Nicklas_MDD_proposal";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.example.org/Nicklas_MDD_proposal";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "Nicklas_MDD_proposal";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Nicklas_MDD_proposalPackage eINSTANCE = Nicklas_MDD_proposal.impl.Nicklas_MDD_proposalPackageImpl.init();

	/**
	 * The meta object id for the '{@link Nicklas_MDD_proposal.impl.ModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Nicklas_MDD_proposal.impl.ModelImpl
	 * @see Nicklas_MDD_proposal.impl.Nicklas_MDD_proposalPackageImpl#getModel()
	 * @generated
	 */
	int MODEL = 0;

	/**
	 * The feature id for the '<em><b>Root</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__ROOT = 0;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link Nicklas_MDD_proposal.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Nicklas_MDD_proposal.impl.NamedElementImpl
	 * @see Nicklas_MDD_proposal.impl.Nicklas_MDD_proposalPackageImpl#getNamedElement()
	 * @generated
	 */
	int NAMED_ELEMENT = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__NAME = 0;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link Nicklas_MDD_proposal.impl.ComponentImpl <em>Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Nicklas_MDD_proposal.impl.ComponentImpl
	 * @see Nicklas_MDD_proposal.impl.Nicklas_MDD_proposalPackageImpl#getComponent()
	 * @generated
	 */
	int COMPONENT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__TYPE = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__VALUE = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Depends On</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__DEPENDS_ON = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Available Options</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__AVAILABLE_OPTIONS = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Nicklas_MDD_proposal.impl.DependencyImpl <em>Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Nicklas_MDD_proposal.impl.DependencyImpl
	 * @see Nicklas_MDD_proposal.impl.Nicklas_MDD_proposalPackageImpl#getDependency()
	 * @generated
	 */
	int DEPENDENCY = 2;

	/**
	 * The number of structural features of the '<em>Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link Nicklas_MDD_proposal.impl.BiDepImpl <em>Bi Dep</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Nicklas_MDD_proposal.impl.BiDepImpl
	 * @see Nicklas_MDD_proposal.impl.Nicklas_MDD_proposalPackageImpl#getBiDep()
	 * @generated
	 */
	int BI_DEP = 3;

	/**
	 * The feature id for the '<em><b>Left Dep</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BI_DEP__LEFT_DEP = DEPENDENCY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right Dep</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BI_DEP__RIGHT_DEP = DEPENDENCY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Op</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BI_DEP__OP = DEPENDENCY_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Bi Dep</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BI_DEP_FEATURE_COUNT = DEPENDENCY_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Bi Dep</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BI_DEP_OPERATION_COUNT = DEPENDENCY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Nicklas_MDD_proposal.impl.UnDepImpl <em>Un Dep</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Nicklas_MDD_proposal.impl.UnDepImpl
	 * @see Nicklas_MDD_proposal.impl.Nicklas_MDD_proposalPackageImpl#getUnDep()
	 * @generated
	 */
	int UN_DEP = 4;

	/**
	 * The feature id for the '<em><b>Dep</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UN_DEP__DEP = DEPENDENCY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Op</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UN_DEP__OP = DEPENDENCY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Un Dep</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UN_DEP_FEATURE_COUNT = DEPENDENCY_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Un Dep</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UN_DEP_OPERATION_COUNT = DEPENDENCY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Nicklas_MDD_proposal.impl.ConstImpl <em>Const</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Nicklas_MDD_proposal.impl.ConstImpl
	 * @see Nicklas_MDD_proposal.impl.Nicklas_MDD_proposalPackageImpl#getConst()
	 * @generated
	 */
	int CONST = 6;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONST__TYPE = DEPENDENCY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONST__VALUE = DEPENDENCY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONST__COMPONENT = DEPENDENCY_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Const</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONST_FEATURE_COUNT = DEPENDENCY_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Const</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONST_OPERATION_COUNT = DEPENDENCY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Nicklas_MDD_proposal.impl.ValuedElementImpl <em>Valued Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Nicklas_MDD_proposal.impl.ValuedElementImpl
	 * @see Nicklas_MDD_proposal.impl.Nicklas_MDD_proposalPackageImpl#getValuedElement()
	 * @generated
	 */
	int VALUED_ELEMENT = 7;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUED_ELEMENT__TYPE = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUED_ELEMENT__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Valued Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUED_ELEMENT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Valued Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link Nicklas_MDD_proposal.impl.DependencyStatementImpl <em>Dependency Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Nicklas_MDD_proposal.impl.DependencyStatementImpl
	 * @see Nicklas_MDD_proposal.impl.Nicklas_MDD_proposalPackageImpl#getDependencyStatement()
	 * @generated
	 */
	int DEPENDENCY_STATEMENT = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY_STATEMENT__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Dependencies</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY_STATEMENT__DEPENDENCIES = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Dependency Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY_STATEMENT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Dependency Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY_STATEMENT_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Nicklas_MDD_proposal.impl.OptionsImpl <em>Options</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Nicklas_MDD_proposal.impl.OptionsImpl
	 * @see Nicklas_MDD_proposal.impl.Nicklas_MDD_proposalPackageImpl#getOptions()
	 * @generated
	 */
	int OPTIONS = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTIONS__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Components</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTIONS__COMPONENTS = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Display Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTIONS__DISPLAY_TYPE = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Options</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTIONS_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Options</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTIONS_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Nicklas_MDD_proposal.SimpleTypes <em>Simple Types</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Nicklas_MDD_proposal.SimpleTypes
	 * @see Nicklas_MDD_proposal.impl.Nicklas_MDD_proposalPackageImpl#getSimpleTypes()
	 * @generated
	 */
	int SIMPLE_TYPES = 10;

	/**
	 * The meta object id for the '{@link Nicklas_MDD_proposal.DisplayType <em>Display Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Nicklas_MDD_proposal.DisplayType
	 * @see Nicklas_MDD_proposal.impl.Nicklas_MDD_proposalPackageImpl#getDisplayType()
	 * @generated
	 */
	int DISPLAY_TYPE = 11;

	/**
	 * The meta object id for the '{@link Nicklas_MDD_proposal.BinOperator <em>Bin Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Nicklas_MDD_proposal.BinOperator
	 * @see Nicklas_MDD_proposal.impl.Nicklas_MDD_proposalPackageImpl#getBinOperator()
	 * @generated
	 */
	int BIN_OPERATOR = 12;

	/**
	 * The meta object id for the '{@link Nicklas_MDD_proposal.UnOperator <em>Un Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Nicklas_MDD_proposal.UnOperator
	 * @see Nicklas_MDD_proposal.impl.Nicklas_MDD_proposalPackageImpl#getUnOperator()
	 * @generated
	 */
	int UN_OPERATOR = 13;


	/**
	 * Returns the meta object for class '{@link Nicklas_MDD_proposal.Model <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see Nicklas_MDD_proposal.Model
	 * @generated
	 */
	EClass getModel();

	/**
	 * Returns the meta object for the containment reference list '{@link Nicklas_MDD_proposal.Model#getRoot <em>Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Root</em>'.
	 * @see Nicklas_MDD_proposal.Model#getRoot()
	 * @see #getModel()
	 * @generated
	 */
	EReference getModel_Root();

	/**
	 * Returns the meta object for class '{@link Nicklas_MDD_proposal.Component <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component</em>'.
	 * @see Nicklas_MDD_proposal.Component
	 * @generated
	 */
	EClass getComponent();

	/**
	 * Returns the meta object for the containment reference list '{@link Nicklas_MDD_proposal.Component#getAvailableOptions <em>Available Options</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Available Options</em>'.
	 * @see Nicklas_MDD_proposal.Component#getAvailableOptions()
	 * @see #getComponent()
	 * @generated
	 */
	EReference getComponent_AvailableOptions();

	/**
	 * Returns the meta object for the containment reference list '{@link Nicklas_MDD_proposal.Component#getDependsOn <em>Depends On</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Depends On</em>'.
	 * @see Nicklas_MDD_proposal.Component#getDependsOn()
	 * @see #getComponent()
	 * @generated
	 */
	EReference getComponent_DependsOn();

	/**
	 * Returns the meta object for class '{@link Nicklas_MDD_proposal.Dependency <em>Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dependency</em>'.
	 * @see Nicklas_MDD_proposal.Dependency
	 * @generated
	 */
	EClass getDependency();

	/**
	 * Returns the meta object for class '{@link Nicklas_MDD_proposal.BiDep <em>Bi Dep</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bi Dep</em>'.
	 * @see Nicklas_MDD_proposal.BiDep
	 * @generated
	 */
	EClass getBiDep();

	/**
	 * Returns the meta object for the containment reference '{@link Nicklas_MDD_proposal.BiDep#getLeftDep <em>Left Dep</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left Dep</em>'.
	 * @see Nicklas_MDD_proposal.BiDep#getLeftDep()
	 * @see #getBiDep()
	 * @generated
	 */
	EReference getBiDep_LeftDep();

	/**
	 * Returns the meta object for the containment reference '{@link Nicklas_MDD_proposal.BiDep#getRightDep <em>Right Dep</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right Dep</em>'.
	 * @see Nicklas_MDD_proposal.BiDep#getRightDep()
	 * @see #getBiDep()
	 * @generated
	 */
	EReference getBiDep_RightDep();

	/**
	 * Returns the meta object for the attribute '{@link Nicklas_MDD_proposal.BiDep#getOp <em>Op</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Op</em>'.
	 * @see Nicklas_MDD_proposal.BiDep#getOp()
	 * @see #getBiDep()
	 * @generated
	 */
	EAttribute getBiDep_Op();

	/**
	 * Returns the meta object for class '{@link Nicklas_MDD_proposal.UnDep <em>Un Dep</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Un Dep</em>'.
	 * @see Nicklas_MDD_proposal.UnDep
	 * @generated
	 */
	EClass getUnDep();

	/**
	 * Returns the meta object for the containment reference '{@link Nicklas_MDD_proposal.UnDep#getDep <em>Dep</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Dep</em>'.
	 * @see Nicklas_MDD_proposal.UnDep#getDep()
	 * @see #getUnDep()
	 * @generated
	 */
	EReference getUnDep_Dep();

	/**
	 * Returns the meta object for the attribute '{@link Nicklas_MDD_proposal.UnDep#getOp <em>Op</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Op</em>'.
	 * @see Nicklas_MDD_proposal.UnDep#getOp()
	 * @see #getUnDep()
	 * @generated
	 */
	EAttribute getUnDep_Op();

	/**
	 * Returns the meta object for class '{@link Nicklas_MDD_proposal.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see Nicklas_MDD_proposal.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link Nicklas_MDD_proposal.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see Nicklas_MDD_proposal.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for class '{@link Nicklas_MDD_proposal.Const <em>Const</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Const</em>'.
	 * @see Nicklas_MDD_proposal.Const
	 * @generated
	 */
	EClass getConst();

	/**
	 * Returns the meta object for the reference '{@link Nicklas_MDD_proposal.Const#getComponent <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Component</em>'.
	 * @see Nicklas_MDD_proposal.Const#getComponent()
	 * @see #getConst()
	 * @generated
	 */
	EReference getConst_Component();

	/**
	 * Returns the meta object for class '{@link Nicklas_MDD_proposal.ValuedElement <em>Valued Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Valued Element</em>'.
	 * @see Nicklas_MDD_proposal.ValuedElement
	 * @generated
	 */
	EClass getValuedElement();

	/**
	 * Returns the meta object for the attribute '{@link Nicklas_MDD_proposal.ValuedElement#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see Nicklas_MDD_proposal.ValuedElement#getType()
	 * @see #getValuedElement()
	 * @generated
	 */
	EAttribute getValuedElement_Type();

	/**
	 * Returns the meta object for the attribute '{@link Nicklas_MDD_proposal.ValuedElement#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see Nicklas_MDD_proposal.ValuedElement#getValue()
	 * @see #getValuedElement()
	 * @generated
	 */
	EAttribute getValuedElement_Value();

	/**
	 * Returns the meta object for class '{@link Nicklas_MDD_proposal.DependencyStatement <em>Dependency Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dependency Statement</em>'.
	 * @see Nicklas_MDD_proposal.DependencyStatement
	 * @generated
	 */
	EClass getDependencyStatement();

	/**
	 * Returns the meta object for the containment reference '{@link Nicklas_MDD_proposal.DependencyStatement#getDependencies <em>Dependencies</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Dependencies</em>'.
	 * @see Nicklas_MDD_proposal.DependencyStatement#getDependencies()
	 * @see #getDependencyStatement()
	 * @generated
	 */
	EReference getDependencyStatement_Dependencies();

	/**
	 * Returns the meta object for class '{@link Nicklas_MDD_proposal.Options <em>Options</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Options</em>'.
	 * @see Nicklas_MDD_proposal.Options
	 * @generated
	 */
	EClass getOptions();

	/**
	 * Returns the meta object for the containment reference list '{@link Nicklas_MDD_proposal.Options#getComponents <em>Components</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Components</em>'.
	 * @see Nicklas_MDD_proposal.Options#getComponents()
	 * @see #getOptions()
	 * @generated
	 */
	EReference getOptions_Components();

	/**
	 * Returns the meta object for the attribute '{@link Nicklas_MDD_proposal.Options#getDisplayType <em>Display Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Display Type</em>'.
	 * @see Nicklas_MDD_proposal.Options#getDisplayType()
	 * @see #getOptions()
	 * @generated
	 */
	EAttribute getOptions_DisplayType();

	/**
	 * Returns the meta object for enum '{@link Nicklas_MDD_proposal.SimpleTypes <em>Simple Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Simple Types</em>'.
	 * @see Nicklas_MDD_proposal.SimpleTypes
	 * @generated
	 */
	EEnum getSimpleTypes();

	/**
	 * Returns the meta object for enum '{@link Nicklas_MDD_proposal.DisplayType <em>Display Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Display Type</em>'.
	 * @see Nicklas_MDD_proposal.DisplayType
	 * @generated
	 */
	EEnum getDisplayType();

	/**
	 * Returns the meta object for enum '{@link Nicklas_MDD_proposal.BinOperator <em>Bin Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Bin Operator</em>'.
	 * @see Nicklas_MDD_proposal.BinOperator
	 * @generated
	 */
	EEnum getBinOperator();

	/**
	 * Returns the meta object for enum '{@link Nicklas_MDD_proposal.UnOperator <em>Un Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Un Operator</em>'.
	 * @see Nicklas_MDD_proposal.UnOperator
	 * @generated
	 */
	EEnum getUnOperator();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Nicklas_MDD_proposalFactory getNicklas_MDD_proposalFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link Nicklas_MDD_proposal.impl.ModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Nicklas_MDD_proposal.impl.ModelImpl
		 * @see Nicklas_MDD_proposal.impl.Nicklas_MDD_proposalPackageImpl#getModel()
		 * @generated
		 */
		EClass MODEL = eINSTANCE.getModel();

		/**
		 * The meta object literal for the '<em><b>Root</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL__ROOT = eINSTANCE.getModel_Root();

		/**
		 * The meta object literal for the '{@link Nicklas_MDD_proposal.impl.ComponentImpl <em>Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Nicklas_MDD_proposal.impl.ComponentImpl
		 * @see Nicklas_MDD_proposal.impl.Nicklas_MDD_proposalPackageImpl#getComponent()
		 * @generated
		 */
		EClass COMPONENT = eINSTANCE.getComponent();

		/**
		 * The meta object literal for the '<em><b>Available Options</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT__AVAILABLE_OPTIONS = eINSTANCE.getComponent_AvailableOptions();

		/**
		 * The meta object literal for the '<em><b>Depends On</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT__DEPENDS_ON = eINSTANCE.getComponent_DependsOn();

		/**
		 * The meta object literal for the '{@link Nicklas_MDD_proposal.impl.DependencyImpl <em>Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Nicklas_MDD_proposal.impl.DependencyImpl
		 * @see Nicklas_MDD_proposal.impl.Nicklas_MDD_proposalPackageImpl#getDependency()
		 * @generated
		 */
		EClass DEPENDENCY = eINSTANCE.getDependency();

		/**
		 * The meta object literal for the '{@link Nicklas_MDD_proposal.impl.BiDepImpl <em>Bi Dep</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Nicklas_MDD_proposal.impl.BiDepImpl
		 * @see Nicklas_MDD_proposal.impl.Nicklas_MDD_proposalPackageImpl#getBiDep()
		 * @generated
		 */
		EClass BI_DEP = eINSTANCE.getBiDep();

		/**
		 * The meta object literal for the '<em><b>Left Dep</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BI_DEP__LEFT_DEP = eINSTANCE.getBiDep_LeftDep();

		/**
		 * The meta object literal for the '<em><b>Right Dep</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BI_DEP__RIGHT_DEP = eINSTANCE.getBiDep_RightDep();

		/**
		 * The meta object literal for the '<em><b>Op</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BI_DEP__OP = eINSTANCE.getBiDep_Op();

		/**
		 * The meta object literal for the '{@link Nicklas_MDD_proposal.impl.UnDepImpl <em>Un Dep</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Nicklas_MDD_proposal.impl.UnDepImpl
		 * @see Nicklas_MDD_proposal.impl.Nicklas_MDD_proposalPackageImpl#getUnDep()
		 * @generated
		 */
		EClass UN_DEP = eINSTANCE.getUnDep();

		/**
		 * The meta object literal for the '<em><b>Dep</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UN_DEP__DEP = eINSTANCE.getUnDep_Dep();

		/**
		 * The meta object literal for the '<em><b>Op</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UN_DEP__OP = eINSTANCE.getUnDep_Op();

		/**
		 * The meta object literal for the '{@link Nicklas_MDD_proposal.impl.NamedElementImpl <em>Named Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Nicklas_MDD_proposal.impl.NamedElementImpl
		 * @see Nicklas_MDD_proposal.impl.Nicklas_MDD_proposalPackageImpl#getNamedElement()
		 * @generated
		 */
		EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '{@link Nicklas_MDD_proposal.impl.ConstImpl <em>Const</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Nicklas_MDD_proposal.impl.ConstImpl
		 * @see Nicklas_MDD_proposal.impl.Nicklas_MDD_proposalPackageImpl#getConst()
		 * @generated
		 */
		EClass CONST = eINSTANCE.getConst();

		/**
		 * The meta object literal for the '<em><b>Component</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONST__COMPONENT = eINSTANCE.getConst_Component();

		/**
		 * The meta object literal for the '{@link Nicklas_MDD_proposal.impl.ValuedElementImpl <em>Valued Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Nicklas_MDD_proposal.impl.ValuedElementImpl
		 * @see Nicklas_MDD_proposal.impl.Nicklas_MDD_proposalPackageImpl#getValuedElement()
		 * @generated
		 */
		EClass VALUED_ELEMENT = eINSTANCE.getValuedElement();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VALUED_ELEMENT__TYPE = eINSTANCE.getValuedElement_Type();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VALUED_ELEMENT__VALUE = eINSTANCE.getValuedElement_Value();

		/**
		 * The meta object literal for the '{@link Nicklas_MDD_proposal.impl.DependencyStatementImpl <em>Dependency Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Nicklas_MDD_proposal.impl.DependencyStatementImpl
		 * @see Nicklas_MDD_proposal.impl.Nicklas_MDD_proposalPackageImpl#getDependencyStatement()
		 * @generated
		 */
		EClass DEPENDENCY_STATEMENT = eINSTANCE.getDependencyStatement();

		/**
		 * The meta object literal for the '<em><b>Dependencies</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPENDENCY_STATEMENT__DEPENDENCIES = eINSTANCE.getDependencyStatement_Dependencies();

		/**
		 * The meta object literal for the '{@link Nicklas_MDD_proposal.impl.OptionsImpl <em>Options</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Nicklas_MDD_proposal.impl.OptionsImpl
		 * @see Nicklas_MDD_proposal.impl.Nicklas_MDD_proposalPackageImpl#getOptions()
		 * @generated
		 */
		EClass OPTIONS = eINSTANCE.getOptions();

		/**
		 * The meta object literal for the '<em><b>Components</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPTIONS__COMPONENTS = eINSTANCE.getOptions_Components();

		/**
		 * The meta object literal for the '<em><b>Display Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPTIONS__DISPLAY_TYPE = eINSTANCE.getOptions_DisplayType();

		/**
		 * The meta object literal for the '{@link Nicklas_MDD_proposal.SimpleTypes <em>Simple Types</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Nicklas_MDD_proposal.SimpleTypes
		 * @see Nicklas_MDD_proposal.impl.Nicklas_MDD_proposalPackageImpl#getSimpleTypes()
		 * @generated
		 */
		EEnum SIMPLE_TYPES = eINSTANCE.getSimpleTypes();

		/**
		 * The meta object literal for the '{@link Nicklas_MDD_proposal.DisplayType <em>Display Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Nicklas_MDD_proposal.DisplayType
		 * @see Nicklas_MDD_proposal.impl.Nicklas_MDD_proposalPackageImpl#getDisplayType()
		 * @generated
		 */
		EEnum DISPLAY_TYPE = eINSTANCE.getDisplayType();

		/**
		 * The meta object literal for the '{@link Nicklas_MDD_proposal.BinOperator <em>Bin Operator</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Nicklas_MDD_proposal.BinOperator
		 * @see Nicklas_MDD_proposal.impl.Nicklas_MDD_proposalPackageImpl#getBinOperator()
		 * @generated
		 */
		EEnum BIN_OPERATOR = eINSTANCE.getBinOperator();

		/**
		 * The meta object literal for the '{@link Nicklas_MDD_proposal.UnOperator <em>Un Operator</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Nicklas_MDD_proposal.UnOperator
		 * @see Nicklas_MDD_proposal.impl.Nicklas_MDD_proposalPackageImpl#getUnOperator()
		 * @generated
		 */
		EEnum UN_OPERATOR = eINSTANCE.getUnOperator();

	}

} //Nicklas_MDD_proposalPackage
