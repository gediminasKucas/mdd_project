/**
 */
package Nicklas_MDD_proposal;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Un Dep</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link Nicklas_MDD_proposal.UnDep#getDep <em>Dep</em>}</li>
 *   <li>{@link Nicklas_MDD_proposal.UnDep#getOp <em>Op</em>}</li>
 * </ul>
 * </p>
 *
 * @see Nicklas_MDD_proposal.Nicklas_MDD_proposalPackage#getUnDep()
 * @model
 * @generated
 */
public interface UnDep extends Dependency {
	/**
	 * Returns the value of the '<em><b>Dep</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dep</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dep</em>' containment reference.
	 * @see #setDep(Dependency)
	 * @see Nicklas_MDD_proposal.Nicklas_MDD_proposalPackage#getUnDep_Dep()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Dependency getDep();

	/**
	 * Sets the value of the '{@link Nicklas_MDD_proposal.UnDep#getDep <em>Dep</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dep</em>' containment reference.
	 * @see #getDep()
	 * @generated
	 */
	void setDep(Dependency value);

	/**
	 * Returns the value of the '<em><b>Op</b></em>' attribute.
	 * The literals are from the enumeration {@link Nicklas_MDD_proposal.UnOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Op</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Op</em>' attribute.
	 * @see Nicklas_MDD_proposal.UnOperator
	 * @see #setOp(UnOperator)
	 * @see Nicklas_MDD_proposal.Nicklas_MDD_proposalPackage#getUnDep_Op()
	 * @model
	 * @generated
	 */
	UnOperator getOp();

	/**
	 * Sets the value of the '{@link Nicklas_MDD_proposal.UnDep#getOp <em>Op</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Op</em>' attribute.
	 * @see Nicklas_MDD_proposal.UnOperator
	 * @see #getOp()
	 * @generated
	 */
	void setOp(UnOperator value);

} // UnDep
