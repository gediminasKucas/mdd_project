/**
 */
package Nicklas_MDD_proposal;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Valued Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link Nicklas_MDD_proposal.ValuedElement#getType <em>Type</em>}</li>
 *   <li>{@link Nicklas_MDD_proposal.ValuedElement#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see Nicklas_MDD_proposal.Nicklas_MDD_proposalPackage#getValuedElement()
 * @model abstract="true"
 * @generated
 */
public interface ValuedElement extends EObject {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link Nicklas_MDD_proposal.SimpleTypes}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see Nicklas_MDD_proposal.SimpleTypes
	 * @see #setType(SimpleTypes)
	 * @see Nicklas_MDD_proposal.Nicklas_MDD_proposalPackage#getValuedElement_Type()
	 * @model
	 * @generated
	 */
	SimpleTypes getType();

	/**
	 * Sets the value of the '{@link Nicklas_MDD_proposal.ValuedElement#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see Nicklas_MDD_proposal.SimpleTypes
	 * @see #getType()
	 * @generated
	 */
	void setType(SimpleTypes value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see Nicklas_MDD_proposal.Nicklas_MDD_proposalPackage#getValuedElement_Value()
	 * @model
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link Nicklas_MDD_proposal.ValuedElement#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

} // ValuedElement
