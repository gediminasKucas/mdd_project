package Nicklas_MDD_proposal.validation;

import Nicklas_MDD_proposal.Component;
import Nicklas_MDD_proposal.DisplayType;
import Nicklas_MDD_proposal.Options;
import Nicklas_MDD_proposal.SimpleTypes;
import Nicklas_MDD_proposal.ValuedElement;
import com.google.common.base.Objects;
import java.util.Arrays;
import javax.management.openmbean.SimpleType;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class Constraints {
  protected static boolean _constraint(final Component it) {
    boolean _and = false;
    boolean _and_1 = false;
    boolean _and_2 = false;
    boolean _and_3 = false;
    SimpleTypes _type = it.getType();
    boolean _notEquals = (!Objects.equal(_type, null));
    if (!_notEquals) {
      _and_3 = false;
    } else {
      EList<Options> _availableOptions = it.getAvailableOptions();
      boolean _equals = Objects.equal(_availableOptions, null);
      _and_3 = _equals;
    }
    if (!_and_3) {
      _and_2 = false;
    } else {
      SimpleTypes _type_1 = it.getType();
      boolean _notEquals_1 = (!Objects.equal(_type_1, null));
      _and_2 = _notEquals_1;
    }
    if (!_and_2) {
      _and_1 = false;
    } else {
      String _value = it.getValue();
      boolean _notEquals_2 = (!Objects.equal(_value, null));
      _and_1 = _notEquals_2;
    }
    if (!_and_1) {
      _and = false;
    } else {
      boolean _and_4 = false;
      SimpleTypes _type_2 = it.getType();
      boolean _equals_1 = Objects.equal(_type_2, null);
      if (!_equals_1) {
        _and_4 = false;
      } else {
        boolean _and_5 = false;
        EList<Options> _availableOptions_1 = it.getAvailableOptions();
        boolean _notEquals_3 = (!Objects.equal(_availableOptions_1, null));
        if (!_notEquals_3) {
          _and_5 = false;
        } else {
          EList<Options> _availableOptions_2 = it.getAvailableOptions();
          boolean _isEmpty = _availableOptions_2.isEmpty();
          boolean _not = (!_isEmpty);
          _and_5 = _not;
        }
        _and_4 = _and_5;
      }
      _and = _and_4;
    }
    return _and;
  }
  
  protected static boolean _constraint(final Options it) {
    boolean _and = false;
    EList<Component> _components = it.getComponents();
    final Function1<Component, Boolean> _function = new Function1<Component, Boolean>() {
      public Boolean apply(final Component x) {
        SimpleTypes _type = x.getType();
        EList<Component> _components = it.getComponents();
        Component _get = _components.get(0);
        SimpleTypes _type_1 = _get.getType();
        return Boolean.valueOf(Objects.equal(_type, _type_1));
      }
    };
    boolean _forall = IterableExtensions.<Component>forall(_components, _function);
    if (!_forall) {
      _and = false;
    } else {
      boolean _and_1 = false;
      boolean _and_2 = false;
      boolean _and_3 = false;
      DisplayType _displayType = it.getDisplayType();
      boolean _equals = Objects.equal(_displayType, DisplayType.RANGE);
      if (!_equals) {
        _and_3 = false;
      } else {
        EList<Component> _components_1 = it.getComponents();
        int _size = _components_1.size();
        boolean _lessEqualsThan = (_size <= 3);
        _and_3 = _lessEqualsThan;
      }
      if (!_and_3) {
        _and_2 = false;
      } else {
        EList<Component> _components_2 = it.getComponents();
        int _size_1 = _components_2.size();
        boolean _greaterEqualsThan = (_size_1 >= 2);
        _and_2 = _greaterEqualsThan;
      }
      if (!_and_2) {
        _and_1 = false;
      } else {
        EList<Component> _components_3 = it.getComponents();
        final Function1<Component, Boolean> _function_1 = new Function1<Component, Boolean>() {
          public Boolean apply(final Component x) {
            SimpleTypes _type = x.getType();
            return Boolean.valueOf(Objects.equal(_type, SimpleType.INTEGER));
          }
        };
        boolean _forall_1 = IterableExtensions.<Component>forall(_components_3, _function_1);
        _and_1 = _forall_1;
      }
      _and = _and_1;
    }
    return _and;
  }
  
  protected static boolean _constraint(final ValuedElement it) {
    boolean _and = false;
    boolean _and_1 = false;
    boolean _and_2 = false;
    boolean _and_3 = false;
    boolean _and_4 = false;
    boolean _and_5 = false;
    boolean _and_6 = false;
    boolean _and_7 = false;
    boolean _and_8 = false;
    SimpleTypes _type = it.getType();
    boolean _equals = Objects.equal(_type, SimpleTypes.BOOLEAN);
    if (!_equals) {
      _and_8 = false;
    } else {
      boolean _or = false;
      String _value = it.getValue();
      boolean _equalsIgnoreCase = _value.equalsIgnoreCase("true");
      if (_equalsIgnoreCase) {
        _or = true;
      } else {
        String _value_1 = it.getValue();
        boolean _equalsIgnoreCase_1 = _value_1.equalsIgnoreCase("false");
        _or = _equalsIgnoreCase_1;
      }
      _and_8 = _or;
    }
    if (!_and_8) {
      _and_7 = false;
    } else {
      SimpleTypes _type_1 = it.getType();
      boolean _equals_1 = Objects.equal(_type_1, SimpleTypes.INT);
      _and_7 = _equals_1;
    }
    if (!_and_7) {
      _and_6 = false;
    } else {
      String _value_2 = it.getValue();
      boolean _isInt = Constraints.isInt(_value_2);
      _and_6 = _isInt;
    }
    if (!_and_6) {
      _and_5 = false;
    } else {
      SimpleTypes _type_2 = it.getType();
      boolean _equals_2 = Objects.equal(_type_2, SimpleTypes.DOUBLE);
      _and_5 = _equals_2;
    }
    if (!_and_5) {
      _and_4 = false;
    } else {
      String _value_3 = it.getValue();
      boolean _isDouble = Constraints.isDouble(_value_3);
      _and_4 = _isDouble;
    }
    if (!_and_4) {
      _and_3 = false;
    } else {
      SimpleTypes _type_3 = it.getType();
      boolean _equals_3 = Objects.equal(_type_3, SimpleTypes.FLOAT);
      _and_3 = _equals_3;
    }
    if (!_and_3) {
      _and_2 = false;
    } else {
      String _value_4 = it.getValue();
      boolean _isFloat = Constraints.isFloat(_value_4);
      _and_2 = _isFloat;
    }
    if (!_and_2) {
      _and_1 = false;
    } else {
      SimpleTypes _type_4 = it.getType();
      boolean _equals_4 = Objects.equal(_type_4, SimpleTypes.CHAR);
      _and_1 = _equals_4;
    }
    if (!_and_1) {
      _and = false;
    } else {
      String _value_5 = it.getValue();
      int _length = _value_5.length();
      boolean _equals_5 = (_length == 1);
      _and = _equals_5;
    }
    return _and;
  }
  
  public static boolean isInt(final String value) {
    try {
      Integer.parseInt(value);
    } catch (final Throwable _t) {
      if (_t instanceof NumberFormatException) {
        final NumberFormatException e = (NumberFormatException)_t;
        return false;
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
    return true;
  }
  
  public static boolean isDouble(final String value) {
    try {
      Double.parseDouble(value);
    } catch (final Throwable _t) {
      if (_t instanceof NumberFormatException) {
        final NumberFormatException e = (NumberFormatException)_t;
        return false;
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
    return true;
  }
  
  public static boolean isFloat(final String value) {
    try {
      Float.parseFloat(value);
    } catch (final Throwable _t) {
      if (_t instanceof NumberFormatException) {
        final NumberFormatException e = (NumberFormatException)_t;
        return false;
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
    return true;
  }
  
  public static boolean constraint(final EObject it) {
    if (it instanceof Component) {
      return _constraint((Component)it);
    } else if (it instanceof Options) {
      return _constraint((Options)it);
    } else if (it instanceof ValuedElement) {
      return _constraint((ValuedElement)it);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(it).toString());
    }
  }
}
